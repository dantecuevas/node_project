CREATE TABLE users (
    id serial primary key,
    user_name varchar(64),
    first_name varchar(64),
    last_name varchar(64),
    email varchar(64),
    password varchar(128),
    birthdate date,
    gender boolean,
    created timestamp with time zone DEFAULT now(),
    active boolean default false,
    is_company boolean default false,
    organization_name varchar(84),
    address varchar(84),
    ruc varchar(16),
    phone varchar(40),
    token varchar(64),
    country_id integer,
    state_id integer,
    city_id integer,
    role_id integer
);

CREATE TABLE roles (
    id serial primary key,
    name varchar(64)
);

CREATE TABLE countries (
    id serial primary key,
    name character varying(128),
    created timestamp with time zone DEFAULT now()
);

CREATE TABLE states (
    id serial primary key,
    name character varying(128),
    country_id integer,
    created timestamp with time zone DEFAULT now()
);
CREATE TABLE cities (
    id serial primary key,
    name character varying(128),
    state_id integer,
    created timestamp with time zone DEFAULT now()
);



CREATE TABLE devices (
    id serial primary key,
    code character varying(64),
    rssitx integer,
    rssirx integer,
    battery integer,
    latitude varchar(80),
    longitude varchar(80),
    state boolean default false,
    created timestamp with time zone DEFAULT now(),
    farmer_id integer,
    type_id integer,
    group_id integer
);

CREATE TABLE types (
    id serial primary key,
    name character varying(128)
);


alter table devices add column farmer_id int;
alter table devices add column validate boolean default false;

alter table devices rename column validate to assign;

/*second changes*/

alter table devices add column group_id int;

CREATE TABLE groups(
    id serial primary key,
    name character varying(64),
    date_start timestamp,
    date_end timestamp,
    farmer_id integer
);

/*third changes*/

alter table devices add column rssitx integer;
alter table devices add column rssirx integer;
alter table devices add column battery integer;
alter table devices add column latitude varchar(80);
alter table devices add column longitude varchar(80);
alter table devices add column state boolean default false;


CREATE TABLE variables(
    id serial primary key,
    name character varying(80),
    device_id integer
);

CREATE TABLE details(
    id serial primary key,
    val_max integer,
    val_min integer,
    multiplier integer,
    variable_id integer
);
CREATE TABLE gateways(
    id serial primary key,
    rssitx integer,
    rssirx integer,
    battery integer,
    latitude varchar(80),
    longitude varchar(80),
    date_read timestamp,
    state boolean,
    device_id integer
);
CREATE TABLE valves(
    id serial primary key,
    rssitx integer,
    rssirx integer,
    battery integer,
    latitude varchar(80),
    longitude varchar(80),
    date_read timestamp,
    state boolean,
    gateway_id integer,
    device_id integer
);

CREATE TABLE sensors(
    id serial primary key,
    rssitx integer,
    rssirx integer,
    battery integer,
    latitude varchar(80),
    longitude varchar(80),
    state boolean,
    tensiometer1 integer,
    tensiometer2 integer,
    tensiometer3 integer,
    tensiometer4 integer,
    conductivity integer,
    temperature integer,
    volume_content integer,
    temperature_station integer,
    humidity integer,
    rain integer,
    speed_wind integer,
    light integer,
    pressure_air integer,
    battery2 integer,
    date_read timestamp,
    gateway_id integer,
    device_id integer
);


/*4 changes*/

CREATE TABLE readings(
    id serial primary key,
    rssitx integer,
    rssirx integer,
    battery integer,
    latitude varchar(80),
    longitude varchar(80),
    state boolean,
    tensiometer1 integer,
    tensiometer2 integer,
    tensiometer3 integer,
    tensiometer4 integer,
    conductivity integer,
    temperature integer,
    volume_content integer,
    temperature_station integer,
    humidity integer,
    rain integer,
    speed_wind integer,
    light integer,
    pressure_air integer,
    battery2 integer,
    date_read timestamp,
    code integer,
    gateway_id integer,
    device_id integer
);
/*5 changes*/

alter table details add column variable integer;
alter table details add column activate boolean;
alter table details add column device_id integer;

/*6 changes*/

alter table devices add column device_idmdb varchar(100);
alter table readings add column dateonly date;
alter table details add column depth integer;

/*7 changes*/
ALTER TABLE devices DROP COLUMN rssitx;
ALTER TABLE devices DROP COLUMN rssirx;
ALTER TABLE devices DROP COLUMN battery;

alter table devices add column pm numeric(10,3);
alter table devices add column vr numeric(10,3);
alter table devices add column cdd numeric(10,3);

/*8 changes*/
CREATE TABLE irrigations(
    id serial primary key,
    name varchar(80),
    description varchar(255),
    date_start timestamp,
    date_end timestamp,
    state boolean,
    farmer_id integer
);

alter table groups add column irrigation_id integer;


CREATE TABLE events(
    id serial primary key,
    date_start timestamp,
    date_end timestamp,
    repeat_type varchar(80),
    group_id integer
);

alter table events add column time_start time;
alter table events add column time_end time;
alter table events add column day integer;