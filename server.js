import http from 'http';
import favicon from 'serve-favicon';

import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import expressSession from 'express-session';
import passport from 'passport';
import sha1 from 'sha1';
import cors from 'cors';
import moment from 'moment';
import {Strategy as LocalStrategy} from 'passport-local';
import nodemailer from 'nodemailer';
import mongoose from 'mongoose';
import config from './app/configs/config';
import helper from './app/configs/helper';
import routes from './app/utils/routes'

const port = process.env.PORT || config.location.port;
const app = express();

const Router = express.Router();

import Sequelize from 'sequelize';
const Op = Sequelize.Op;
const sequelize = new Sequelize(config.db.name, config.db.user, config.db.pass, {
  host: config.db.host,
  dialect: config.db.dialect,
  operatorsAliases: Op,
  //operatorsAliases: operatorsAliases,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

var transporter = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    secure: config.mail.secure, // use SSL
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
});

const DB = {sequelize: sequelize};

sequelize
   .authenticate()
   .then(() => {
      console.log('Connection has been established successfully.');
   })
   .catch(err => {
      console.error('Unable to connect to the database:', err);
   });

mongoose.connect('mongodb://localhost/db_agrintell', { useNewUrlParser: true });
app.set('views', __dirname + '/app/views');
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/images/icons.jpg'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(expressSession({
   secret: '8rhfr8rfhr987fhdj9krxd0r4f0ldk843kurdjy4ff4',
   resave: false,
   name: "tacho_admin",
   saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/', express.static(__dirname + '/public'));

app.get('/test', function (req, res, next) {
  res.json({msg: 'Enviado desde el Servidor'})
})

global.uri = 'http://127.0.0.1:3000';
global.uri2 = function(text) {
   //console.log("uriconsole");
   let $result = "urirespuesta"+text;
   return $result
}

import UserModel from './app/models/user';
import RoleModel from './app/models/role';
let User = UserModel(sequelize);
let Role = RoleModel(sequelize);
User.belongsTo(Role, { foreignKey: 'role_id' });


let localStrategy = new LocalStrategy( (email, password, done) => {
 

   let $params = {
      where: {email: email, password: sha1(password)},
      include: [{
          model: Role
      }]
   };

  User.findOne($params).then(user => {
      if(!user) {
        return done(null, false, {msg: 'Usuario y Contraseña incorrectos'});    
      }
      if(!user.active) {
          return done(null, false, {msg: 'La cuenta del Usuario no esta activada'});
      }
      return done(null, user, {msg: 'login'});  
   }).catch(function (err) {
      // handle error;
       done(null, false, {msg: 'Error'});
    });
});

passport.use(localStrategy);
passport.serializeUser((user, done) => { done(null, user) });
passport.deserializeUser((user, done) => { done(null, user) });

let location = `${config.location.url}:${config.location.port}`;
let template = {
   view: {
      script: function (array, type){
         let script="";
         for (var i = 0; i < array.length; i++) {
            if(type=='css'){
               script += `<link type="text/css" rel="stylesheet" href="${location}/css/${array[i]}">`
            }
            else if(type=='css1'){
               script += `<link type="text/css" rel="stylesheet" href="${location}/css/${array[i]}" media="print">`
            }
            else if(type=='js'){
               script += `<script src="${location}/js/${array[i]}"> </script>`
            }
            else if(type=='ico'){
               script += `<link rel="shortcut icon" href="${location}/img/${array[i]}">`
            }
         }
         return script
      },
      endpoint: function (endpoint){
         return location+endpoint;
      },
      flash: {
         type: false,
         msg: false,
         /*get: function(type, msg){
            if(type=='success'){
               return `<div class="alert alert-success alert-msg">
                  <strong>Exito!</strong> ${msg}.
               </div>`   
            }
            else if(type=='error'){
               return `<div class="alert alert-danger alert-msg">
                  <strong>Error!</strong> ${msg}.
               </div>`   
            }
         },*/
         get: function(view){
            let html = ``;
            if(view.flash.type=='success'){
               html = `<div class="alert alert-success alert-dismissible" >
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >×</button>
                      <strong><i class="icon fa fa-check"></i> Exito!</strong> ${view.flash.msg}.
                      </div>`   
            }
            else if(view.flash.type=='error'){
               html = `<div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >×</button>
                      <strong><i class="icon fa fa-ban"></i> Error!</strong> ${view.flash.msg}.
                  
               </div>`   
            }
            else if(view.flash.type=='warning'){
               html = `<div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >×</button>
                      <strong><i class="icon fa fa-warning"></i> Aviso!</strong> ${view.flash.msg}.
                  
               </div>`
            }   
            view.flash.type = false;
            view.flash.msg = false;
            return html;
         },
         set: function(type, msg, view) {
            view.flash.type = type;
            view.flash.msg = msg;
         }
      }
   },
   layout: {
      default: "layout/default",
      view: "home/index",
      graph: "layout/forgraph",
      exec: function(layout) {
          layout.default = 'layout/default';
          layout.view = 'home/index';
          layout.graph = "layout/forgraph";
      }
   },
    helper: {
      constants: helper,
      vars: { 
        activity: {
          period: {1: "Diario", 2: "Semanal", 3: "Mensual"}
        },
        entity: {
          type: {1: "ER", 2: "EG"}
        }
      },
      func: {
        active_inactive: function(active) {
          return active ? '<label class="text-success">Activo</label>' : '<label class="text-danger">Inactivo</label>';
        },
        yes_no: function(yes) {
          return yes ? '<label class="text-success">Si</label>' : '<label class="text-danger">No</label>';
        },
        status: function(active, status) {
          if (!active) {
            return '<label class="text-danger">Inactivo</label>';
          } else if (status != 100) {
            return '<label class="text-info">Pendiente (' + status + '%)</label>';
          }
          return '<label class="text-success">Activo</label>';
        },
        trans_status: function(status) {
          switch(status) {
              case 1:
                  return '<label class="text-primary">Nuevo</label>';
              case 2:
                  return '<label class="text-default">Corregido</label>';
              case 3:
                  return '<label class="text-danger">Rechazado</label>';
              case 4:
                  return '<label class="text-success">Aceptado</label>';
              default:
                  return '<label class="text-warning">En Revisión</label>';
          }
        },
        text_field: function(field) {
          return (field == '' || !field) ? '- -' : field;
        },
        text_relation_field: function(relation, field) {
          if (relation)
            return (relation[field] == '' || !relation[field]) ? '- -' : relation[field];
          return '- -';
        },
        date_format: function(date) {
          return moment(date).format('DD-MM-YYYY');
        },
        time_format: function(date) {
          return moment(date).format('h:mm:ss a');
        },
        mode: function(mode) {
          return mode == 1 ? 'Recepcion en mi local' : 'Recojo a domicilio';
        },
        verification: function(point, name) {
          return point ? '<label>' + name + ' <span class="text-success"><i class="fa fa-check"></i></span> &nbsp;&nbsp;&nbsp;</label>' : '<label>' + name + ' <span class="text-danger"><i class="fa fa-times"></i></span> &nbsp;&nbsp;&nbsp;</label>';
        }
      }
   },
 
   session : {}
}
let midlewares = {passport: passport, auth: ensureAuth, acl: ensureACL};
let Source = {app: app, DB: DB, transporter: transporter};
//midlewares = {};

//app.use('/', Router);
//Router.use(midlewares.auth);

import controllers from './app/utils/controllers'
controllers(Router, Source, template, midlewares)


function ensureAuth (req, res, next){
 

  if(routes[req.route.path]){
    if(routes[req.route.path].allow){
      return next();
    }
  }

   console.log('>> Iniciando validacion');
   if(req.isAuthenticated()) {
      console.log('LOGIN');
      template.session = {user: req.user}
      return next();
   }
   console.log("NO LOGIN! >> no tiene permisos");
   template.view.flash.set('error', 'No se ha iniciado sesión', template.view);
   return res.redirect('/login');

   //return res.status(401).send({"login": false});
}

import acl from './app/configs/acl';
function ensureACL (req, res, next){

  return next();
  
  if (req.controller in acl.controller) {
    if (req.route.path in acl.controller[req.controller].endpoint && req.method in acl.controller[req.controller].endpoint[req.route.path].method) {
      var methods = acl.controller[req.controller].endpoint[req.route.path].method[req.method];

      if (methods.indexOf(req.user.role_id) > -1) {
        return next();
      } else {
        template.view.flash.set('error', 'No tiene los permisos necesarios', template.view);
        if (req.get('referer') == undefined) {
          if (req.user.role_id == 1 || req.user.role_id==2) {
            return res.redirect('/home/admin');  
          }
          else{
            return res.redirect('/home/customer');  
          }
          
        }
        return res.redirect(req.get('referer'));
      }
    }
  }
  return next();
}


let server = http.createServer(app).listen(port, () => {
   console.log(`El servidor esta levantado en el puerto ${port}`);
});

