
INSTALLATION
------------

branch v1

- instalar librerias y dependencias
	$ npm install
	$ sudo npm install -g nodemon

- crear base de datos, restauramos schema y data
	createdb <db-name>
	psql <db-name> < db/reciklr_schema.sql
	psql <db-name> < db/reciklr_data.sql

- modificar datos de db, mail en archivo app/configs/config.json
  segun sea el caso



EXECUTION
---------

- Levantar proyecto
	$ npm start