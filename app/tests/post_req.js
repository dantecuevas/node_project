var request = require('request');

var $data = {
	"coordinate": {
		"coordinateid": "9999", 
		"code": 3000, 
		"rssitx": 41,
		"rssirx": 42,
		"battery": 42,
		"latitude": 12000,
		"longitude": 12000,
		"state": 1,
		"date": "2013-02-08 23:30:26"
	},
	"device": {
		"devid": "555555", 
		"code": 1000, 
		"rssitx": 45,
		"rssirx": 45,
		"battery": 35,
		"latitude": 12000,
		"longitude": 12000,
		"state": 1,
		"tensiometer1": 1000,
		"tensiometer2": 1000,
		"tensiometer3": 1000,
		"tensiometer4": 1000,
		"conductivity": 10000,
		"temperature": 100,
		"volume_content": 10000,
		"temperature_station": 200,
		"humidity": 1000,
		"rain": 9999,
		"speed_wind": 1000,
		"light": 300,
		"pressure_air": 1100,
		"battery2":80
	}
}
var $params = {
	url:'http://apselom.com:3005/device/addDevices',
	form: $data
}
request.post($params, function(err,httpResponse,body){ 
		//console.log("ERR: ", err);
		console.log("body: ", body);
		//console.log("BODY:", body);
})