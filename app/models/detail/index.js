import Sequelize from 'sequelize';

let detailModel = function(sequelize){

	var Detail = sequelize.define("details", 
		{
			val_max: Sequelize.INTEGER,
			val_min: Sequelize.INTEGER,
			multiplier: Sequelize.INTEGER,
			variable_id: Sequelize.INTEGER,
			variable: Sequelize.INTEGER,
			activate: Sequelize.BOOLEAN,
			device_id: Sequelize.INTEGER,
			depth: Sequelize.INTEGER
		},
		{
			timestamps: false,
		}
	);
	return Detail;
}
export default detailModel



