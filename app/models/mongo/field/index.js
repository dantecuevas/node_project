
import mongoose from 'mongoose';

let FieldSchema = new mongoose.Schema({

	name: {type: String},
	location: {
        type : {type: String, default:"Point"},
        coordinates : [ { type: Number } ]
    },
	farmer_id: {type:Number}
	
});

export default mongoose.model('fields', FieldSchema)


