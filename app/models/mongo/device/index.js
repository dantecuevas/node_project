import mongoose from 'mongoose';

const DeviceSchema = new mongoose.Schema({

  location: {
        type : {type: String, default:"Point"},
        coordinates : [ { type: Number } ]
    },
  polygon_id: {type:String},
  typedevice: {type:Number},
  device_id: {type:String}
  
});

export default mongoose.model('Device', DeviceSchema)
