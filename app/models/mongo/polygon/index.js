
import mongoose from 'mongoose';

let PolygonSchema = new mongoose.Schema({
	
	type: {type: String},
	geometry: {
		type: {
			type:String,
			enum: ['Polygon'],
	    	required: true

		},
		coordinates:{ 
			type:[[[Number]]]
		}
	},
	field_id: {type:String},
	id: {type:String},
	typefield: {type:String},
	dateS: {type:String},
	dateC: {type:String}
    
	
});

export default mongoose.model('Polygon', PolygonSchema)


