import mongoose from 'mongoose';

const PointSchema = new mongoose.Schema({

  location: {
        type : {type: String, default:"Point"},
        coordinates : [ { type: Number } ]
    },
  polygon_id: {type:String},
  detail :{
  	arena: {type: String},
    limo: {type: String},
    arcilla: {type: String},
    nitrogeno: {type: String},
    fosforo: {type: String},
    potasio: {type: String},
    magnesiomacro: {type: String},
    calcio: {type: String},
    sodio: {type: String},
    cic: {type: String},
    sar: {type: String},
    psi: {type: String},
    zinc: {type: String},
    magnesiomicro: {type: String},
    boro: {type: String},
    hierro: {type: String},
    cobre: {type: String}
  }
});

export default mongoose.model('Point', PointSchema)
