import Sequelize from 'sequelize';

let sensorModel = function(sequelize){

	var Sensor = sequelize.define("sensors", 
		{
			rssitx: Sequelize.INTEGER,
		    rssirx: Sequelize.INTEGER,
		    battery: Sequelize.INTEGER,
		    latitude: Sequelize.STRING,
		    longitude: Sequelize.STRING,
		    state: Sequelize.BOOLEAN,
		    tensiometer1: Sequelize.INTEGER,
		    tensiometer2: Sequelize.INTEGER,
		    tensiometer3: Sequelize.INTEGER,
		    tensiometer4: Sequelize.INTEGER,
		    conductivity: Sequelize.INTEGER,
		    temperature: Sequelize.INTEGER,
		    volume_content: Sequelize.INTEGER,
		    temperature_station: Sequelize.INTEGER,
		    humidity: Sequelize.INTEGER,
		    rain: Sequelize.INTEGER,
		    speed_wind: Sequelize.INTEGER,
		    light: Sequelize.INTEGER,
		    pressure_air: Sequelize.INTEGER,
		    battery2: Sequelize.INTEGER,
		    date_read: Sequelize.DATE,
		    gateway_id: Sequelize.INTEGER,
		    device_id: Sequelize.INTEGER
		},
		{
			timestamps: false,
		}
	);
	return Sensor;
}
export default sensorModel



