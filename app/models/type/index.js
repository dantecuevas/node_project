import Sequelize from 'sequelize';

let typeModel = function(sequelize){

	var Type = sequelize.define("type", 
		{
			name: Sequelize.STRING
		},
		{
			timestamps: false
		}
	);
	
	return Type;
}

export default typeModel

