import Sequelize from 'sequelize';

let roleModel = function(sequelize){

	var Role = sequelize.define("role", 
		{
			name: Sequelize.STRING
		},
		{
			timestamps: false
		}
	);
	
	return Role;
}

export default roleModel

