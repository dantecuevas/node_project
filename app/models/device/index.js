import Sequelize from 'sequelize';

let deviceModel = function(sequelize){

	var Device = sequelize.define("device", 
		{
			code: {
				type: Sequelize.STRING,
				validate: {
					notEmpty: {msg: "No se permite campos vacios"},
					isUnique: function(value) {
	                return Device.findOne({
		                    where: {code: value},
		                    attributes: ['id']
		                }).then(result => {
	                        if (result && this.id != result.id) {
	                            throw new Error('El codigo ya esta en uso');
	                        }
	                    });
	            	}
				}
				
			},
			latitude: Sequelize.STRING,
			longitude: Sequelize.STRING,
			state: Sequelize.BOOLEAN,
			type_id: Sequelize.INTEGER,
			created: Sequelize.DATE,
			assign: Sequelize.BOOLEAN,
			farmer_id: Sequelize.INTEGER,
			device_idmdb: Sequelize.STRING,
			pm: Sequelize.DECIMAL(10,3),
			vr: Sequelize.DECIMAL(10,3),
			cdd: Sequelize.DECIMAL(10,3),
		},
		{
			timestamps: false
		}
	);

	return Device;
}

export default deviceModel
