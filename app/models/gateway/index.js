import Sequelize from 'sequelize';

let gatewayModel = function(sequelize){

	var Gateway = sequelize.define("gateways", 
		{
			rssitx: Sequelize.INTEGER,
		    rssirx: Sequelize.INTEGER,
		    battery: Sequelize.INTEGER,
		    latitude: Sequelize.STRING,
		    longitude: Sequelize.STRING,
		    date_read: Sequelize.DATE,
		    state: Sequelize.BOOLEAN,
		    device_id: Sequelize.INTEGER
		},
		{
			timestamps: false,
		}
	);
	return Gateway;
}
export default gatewayModel



