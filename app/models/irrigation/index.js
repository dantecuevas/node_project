import Sequelize from 'sequelize';

let irrigationModel = function(sequelize){

	var Irrigation = sequelize.define("irrigations", 
		{
			name: {
				type: Sequelize.STRING,
				validate: {
					
					isUnique: function(value) {
		                return Irrigation.findOne({
			                    where: {name: value},
			                    attributes: ['id']
			                }).then(result => {
		                        if (result && this.id != result.id) {
		                            throw new Error('El nombre ya esta en uso');
		                        }
		                    });
		            }
				}
			},
			description: Sequelize.STRING,
			date_start: Sequelize.DATE,
			date_end: Sequelize.DATE,
			state: Sequelize.BOOLEAN,
			farmer_id: Sequelize.INTEGER
		},
		{
			timestamps: false,
		}
	);
	return Irrigation;
}
export default irrigationModel



