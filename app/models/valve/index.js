import Sequelize from 'sequelize';

let valveModel = function(sequelize){

	var Valve = sequelize.define("valves", 
		{
			rssitx: Sequelize.INTEGER,
		    rssirx: Sequelize.INTEGER,
		    battery: Sequelize.INTEGER,
		    latitude: Sequelize.STRING,
		    longitude: Sequelize.STRING,
		    date_read: Sequelize.DATE,
		    state: Sequelize.BOOLEAN,
		    getway_id: Sequelize.INTEGER,
		    device_id: Sequelize.INTEGER
		},
		{
			timestamps: false,
		}
	);
	return Valve;
}
export default valveModel



