import Sequelize from 'sequelize';

let groupModel = function(sequelize){

	var Group = sequelize.define("groups", 
		{
			name: {
				type: Sequelize.STRING,
				validate: {
					
					isUnique: function(value) {
		                return Group.findOne({
			                    where: {name: value},
			                    attributes: ['id']
			                }).then(result => {
		                        if (result && this.id != result.id) {
		                            throw new Error('El nombre del grupo ya esta en uso');
		                        }
		                    });
		            }
				}
			},
			date_start: Sequelize.DATE,
			date_end: Sequelize.DATE,
			farmer_id: Sequelize.INTEGER,
			irrigation_id: Sequelize.INTEGER
		},
		{
			timestamps: false,
		}
	);
	return Group;
}
export default groupModel



