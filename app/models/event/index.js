import Sequelize from 'sequelize';

let eventModel = function(sequelize){

	var Event = sequelize.define("events", 
		{
			date_start: Sequelize.DATE,
			date_end: Sequelize.DATE,
			repeat_type: Sequelize.STRING,
			group_id: Sequelize.INTEGER,
			time_start: Sequelize.TIME,
			time_end: Sequelize.TIME,
			day: Sequelize.INTEGER,
		},
		{
			timestamps: false,
		}
	);
	return Event;
}
export default eventModel



