import Sequelize from 'sequelize';

let stateModel = function(sequelize){

	var State = sequelize.define("state", 
		{
			name: {
				type: Sequelize.STRING,
				validate: {notEmpty: {msg: "No se permite campos vacios"}}
			},
			country_id: Sequelize.INTEGER,
			created: Sequelize.DATE,
			
		},
		{
			timestamps: false
		}
	);

	return State;
}

export default stateModel
