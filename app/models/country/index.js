import Sequelize from 'sequelize';

let countryModel = function(sequelize){

	var Country = sequelize.define("country", 
		{
			name: {
				type: Sequelize.STRING,
				validate: {notEmpty: {msg: "No se permite campos vacios"}}
			},
			created: Sequelize.DATE,
			
		},
		{
			timestamps: false
		}
	);

	return Country;
}

export default countryModel
