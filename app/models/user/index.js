import Sequelize from 'sequelize';
import sha1 from 'sha1';

let userModel = function(sequelize){

	var User = sequelize.define("users", 
		{
			user_name: {
				type: Sequelize.STRING
			},
			first_name: {
				type: Sequelize.STRING
				
			},
			last_name: {
				type: Sequelize.STRING
			},
			email: {
				type: Sequelize.STRING,
				validate: {
					notEmpty: {msg: "No se permite campos vacios"},
					isEmail: {msg: "El formato de correo es incorrecto"},
					isUnique: function(value) {
		                return User.findOne({
			                    where: {email: value},
			                    attributes: ['id']
			                }).then(result => {
		                        if (result && this.id != result.id) {
		                            throw new Error('El correo electronico ya esta en uso');
		                        }
		                    });
		            }
				}
			},
			password: {
				type: Sequelize.STRING,
				validate: {notEmpty: {msg: "No se permite campos vacios"}}
			},
			created: Sequelize.DATE,
			active: Sequelize.BOOLEAN,
			is_company:Sequelize.BOOLEAN,
			organization_name: {
				type: Sequelize.STRING,
				validate: {
					
					isUnique: function(value) {
		                return User.findOne({
			                    where: {organization_name: value},
			                    attributes: ['id']
			                }).then(result => {
		                        if (result && this.id != result.id) {
		                            throw new Error('El nombre de la empresa ya esta en uso');
		                        }
		                    });
		            }
				}
			},
			ruc: Sequelize.STRING,
			phone: Sequelize.STRING,
			birthdate:Sequelize.DATE,
			gender:Sequelize.BOOLEAN,
			token: {
				type: Sequelize.STRING,
				validate: {notEmpty: {msg: "No se permite campos vacios"}}
			},
			country_id: Sequelize.INTEGER,
			state_id: Sequelize.INTEGER,
			city_id: Sequelize.INTEGER,
			role_id: Sequelize.INTEGER
		},
		{
			timestamps: false,
			hooks: {
				beforeSave: (user, options) => {
					if('password' in user._changed) {
				    	user.password = sha1(user.password);
				    }
			    }
			},
			defaultScope: {
				attributes: { exclude: ['password'] },
			}/*,
			scopes: {
				withoutPassword: {
			    	attributes: { exclude: ['password'] },
			  	}
			}*/
		}
	);

	return User;
}

export default userModel
