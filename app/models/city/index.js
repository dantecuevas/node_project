import Sequelize from 'sequelize';

let cityModel = function(sequelize){

	var City = sequelize.define("city", 
		{
			name: {
				type: Sequelize.STRING,
				validate: {notEmpty: {msg: "No se permite campos vacios"}}
			},
			state_id: Sequelize.INTEGER,
			created: Sequelize.DATE,
		},
		{
			timestamps: false
		}
	);

	return City;
}

export default cityModel
