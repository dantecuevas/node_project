import Controller from './Controller'

import Sequelize from 'sequelize'
import moment from 'moment'
import Field from "../models/mongo/field";

export default (Src, template, middleware) => class FieldController extends Controller {

   constructor(){
      super()
   }

   async list(req, res, next) {
      
      let data = await Field.find({farmer_id:req.user.id});
      template.data = {fields: data}
      template.layout.view = 'field/list'
      res.render(template.layout.default, template);

   }

   add(req, res, next) {

      template.layout.view = 'field/add'
      res.render(template.layout.default, template);

   }

   addsave(req, res, next) {

      let data = req.body.data;
      //return res.json(data)
      let fieldData = {
         name: data.name,
         farmer_id: req.user.id,
         location: {
            type: 'Point',
            coordinates: [data.lng, data.lat]
         }
      }

      let field = new Field(fieldData);
      field.save((err,doc)=>{
         if(!err){
            template.view.flash.set('success', 'Campo guardado exitosamente', template.view);
            return res.redirect('/farmer/map');
         } else {
            template.view.flash.set('error', 'Ocurrio un error al guardar el campo', template.view);
            return res.redirect('/field/add');
         } 
      })

   }

}
