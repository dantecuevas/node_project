import Controller from './Controller'

import sha1 from 'sha1';
import Sequelize from 'sequelize';
import moment from 'moment';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import config from '../configs/config';
import generator from 'generate-password';
import UserModel from '../models/user';
import RoleModel from "../models/role";
import CountryModel from "../models/country";
import StateModel from "../models/state";
import CityModel from "../models/city";
import Polygon from "../models/mongo/polygon";
import Fields from "../models/mongo/field";
import DevicePoint from "../models/mongo/device";
import Point from "../models/mongo/point";
import DeviceModel from '../models/device';

let User, Role, Country, State, City, Device;

export default (Src, template, middleware) => class UserController extends Controller {

  constructor(){
    super()
    Device = DeviceModel(Src.DB.sequelize);
  	Role = RoleModel(Src.DB.sequelize);
    User = UserModel(Src.DB.sequelize);
    Country = CountryModel(Src.DB.sequelize);
    State = StateModel(Src.DB.sequelize);
    City = CityModel(Src.DB.sequelize);
    User.belongsTo(Role, { foreignKey: 'role_id' });
    User.belongsTo(Country,{foreignKey: 'country_id'});
    User.belongsTo(State,{foreignKey: 'state_id'});
    User.belongsTo(City,{foreignKey: 'city_id'});
  }


 	list(req, res, next) {

    User.findAll({
      where:{role_id:3},
      include:[{model:Role}],
      order:[['id','ASC']]
    }).then(result=>{
       result.forEach(function(e){
          e.dataValues.created = moment(e.dataValues.created).format('Y-MM-DD');
        })
        template.data = {users: result};
        template.layout.view = 'farmer/list';
        res.render(template.layout.default, template);
    });
  }

  view(req, res, next) {

    User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role },{model:Country},{model:State},{model:City}]})
    .then(result => {
      result.dataValues.created = moment(result.dataValues.created).format('Y-MM-DD');
      template.data = result;
      template.layout.view = 'farmer/view';
      res.render(template.layout.default, template);
    });

  }

  async edit(req, res, next) {

      let roles = await Role.findAll({
        order: [['name', 'DESC']]
      });

      User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
      .then(result => {
         template.data = {farmer: result, roles: roles};
         template.layout.view = 'farmer/edit';
        res.render(template.layout.default, template);
      });

  }

  editsave(req, res, next) {

      let $data = req.body.data;
      
      $data.active = ('active' in $data) ? true : false;
      $data.id = req.params.id;
      $data.birthdate = ($data.birthdate==undefined)? null:moment($data.birthdate,'DD-MM-YYYY').format('YYYY-MM-DD');
      User.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios al usuario', template.view);
          return res.redirect('/farmer/list/');
      }).catch(Sequelize.ValidationError, function (msg) { 
          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/farmer/edit/' + req.params.id);
      });

  }
  delete(req, res, next) {
      User.destroy({where: {id: req.params.id}})
      .then(result =>{
          template.view.flash.set('success', 'Se eliminó el usuario', template.view);
         return res.redirect('/farmer/list');
      });

  }
  profile(req, res, next) {

    User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
    .then(result => {
      result.dataValues.created = moment(result.dataValues.created).format('Y-MM-DD');
      template.data = {farmer: result};
      template.layout.view = 'farmer/profile';
      res.render(template.layout.default, template);
    });

  }

  async profilesave(req, res, next) {

      let roles = await Role.findAll({
        where:{id:[1,2]},
        order: [['name', 'DESC']]
      });

      User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
      .then(result => {
         template.data = {farmer: result, roles: roles};
         template.layout.view = 'farmer/profile_edit';
        res.render(template.layout.default, template);
      });

  }

  profile_editsave(req, res, next) {

      let $data = req.body.data;
      $data.active = ('active' in $data) ? true : false;
      $data.id = req.params.id;

      User.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios de su cuenta', template.view);
          return res.redirect('/farmer/profile/' + req.params.id);
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/farmer/profile/edit/' + req.params.id);
      });

  }

  changepassword(req, res, next) {

      template.layout.view = 'farmer/changepassword';
      res.render(template.layout.default, template);

  }

  changepassword_save(req, res, next) {
      
      let $data = req.body.data;
      if($data.newpassword == $data.confirmpassword) {
        User.findOne({
           where: {id: parseInt(req.user.id), password: sha1($data.password)}
        })
        .then(result => {
            if(result) {
                result.password = $data.newpassword;
                result.save()
                .then(result => {
                    template.view.flash.set('success', 'La constraseña fue cambiada', template.view);
                    return res.redirect('/home/farmer');
                }).catch(Sequelize.ValidationError, function (msg) {
                    template.view.flash.set('error', msg.errors[0].message, template.view);
                    return res.redirect('/farmer/changepassword');
                    //return res.status(422).send(err.errors);
                });
            } else {
                template.view.flash.set('error', 'La contraseña actual no coincide', template.view);
                return res.redirect('/farmer/changepassword');
            }
        });
      } else {
          template.view.flash.set('error', 'La nueva contraseña no es igual a la confirmación', template.view);
          return res.redirect('/farmer/changepassword');
      }

  }

  async map(req, res, next) {

    var dataPolygon = await Polygon.findOne({farmer_id:req.user.id});
    var fields = await Fields.find({farmer_id:req.user.id}).sort({_id:'desc'});
    template.data = {dataPolygon:dataPolygon, fields:fields};
    template.layout.view = 'farmer/map'
    res.render(template.layout.default, template);
  }
  async deletePolygonAll(req, res, next){

    let id = req.body.idpolygon;
    let dataupdate = {device_idmdb:null}
    let devicesfind = await DevicePoint.find({polygon_id:id});

    for (var i = 0; i < devicesfind.length; i++) {
      var idg = devicesfind[i]._id;
      let update = await Device.update(dataupdate, {where: {device_idmdb: idg.toString()} } )
    }
    let device = await DevicePoint.deleteMany({polygon_id:id});
    let point = await Point.deleteMany({polygon_id: id});
    let poly = await Polygon.findOneAndRemove({_id: id},(err,data)=>{
      if (!err)
        res.status(200).json({op:1});   
      else{
        
        res.status(200).json({op:0});   
      }
    });
  }

  deleteMakerField(req, res, next){
    
    Polygon.find({field_id:req.body.id},(err,data)=>{
      console.log('+++++++++++++++++')
      console.log(data);
      console.log(data.length);
      if (data.length > 0) {
        res.status(200).json({result:0});   
      }
      else
      {
        let mkr = Fields.findOneAndRemove({_id:req.body.id},(err,doc)=>{
          if (!err) {
              res.status(200).json({return:1});   
            }
        })      
      }
    });
  
    /*let mkr = Fields.findOneAndRemove({_id:req.body.id},(err,doc)=>{
      if (!err) {
        res.status(200).json({op:"success"});   
      }
    })*/
  }
}

