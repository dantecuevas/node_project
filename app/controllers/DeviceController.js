import Controller from './Controller'
import csv from 'express-json2csv';
import http from 'http';
import request from 'request';
import Sequelize from 'sequelize'
import moment from 'moment'
import DeviceModel from '../models/device'
import TypeModel from '../models/type'
import UserModel from '../models/user';
import GroupModel from '../models/group';
import ReadingModel from '../models/reading';
import GatewayModel from '../models/gateway';
import SensorModel from '../models/sensor';
import ValveModel from '../models/valve';
import DetailModel from '../models/detail';
import IrrigationModel from '../models/irrigation';

import extra_config from '../configs/extra_config';

let Device, Type, User, Group, Gateway, Sensor, Valve, Detail, Reading, Irrigation;


export default (Src, template, middleware) => class DeviceController extends Controller {

   constructor(){
      super()
      Src.app.use(csv);
      User = UserModel(Src.DB.sequelize);
      Device = DeviceModel(Src.DB.sequelize);
      Type = TypeModel(Src.DB.sequelize);
      Group = GroupModel(Src.DB.sequelize);
      Reading = ReadingModel(Src.DB.sequelize);
      Gateway = GatewayModel(Src.DB.sequelize);
      Sensor = SensorModel(Src.DB.sequelize);
      Valve = ValveModel(Src.DB.sequelize);
      Detail = DetailModel(Src.DB.sequelize);
      Irrigation = IrrigationModel(Src.DB.sequelize);
      Device.belongsTo(Type, { foreignKey: 'type_id' });
      Device.belongsTo(User, {foreignKey:'farmer_id'});
      Device.belongsTo(Group, {foreignKey:'group_id'});
      Device.hasMany(Detail, {foreignKey: 'device_id'});
      Device.hasMany(Reading, {foreignKey: 'device_id'});
      Group.belongsTo(User, {foreignKey:'farmer_id'});
      Group.hasMany(Device, {foreignKey:'group_id'});
      Irrigation.belongsTo(User, {foreignKey:'farmer_id'});
      Irrigation.hasMany(Group, {foreignKey:'irrigation_id'});
      
   }

   list(req, res, next) {
      Device.findAll({
        include:[{model:Type},{model:User}]
      }).then(result=>{
        
          template.data = {devices: result};
          template.layout.view = 'device/list';
          res.render(template.layout.default, template);
      });
      
   }

  async view(req, res, next) {

    let devices_read = await Reading.findAll({
      order: [['date_read','DESC']],
      where: {
        device_id: req.params.id
      }
    })

    Device.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Type },{model:User}]})
    .then(result => {
      template.data = {device:result, devices_read: devices_read};
      template.layout.view = 'device/view';
      res.render(template.layout.default, template);
    });

  }
  async add(req, res, next) {
      let types = await Type.findAll({});
      template.data = {types: types};
      template.layout.view = 'device/add';
      res.render(template.layout.default, template);

   }
  addsave(req, res, next) {
      let data = req.body.data;
      
      let datajson = [ 
        { val_max: 2000, val_min: 0, multiplier: -1, variable: 1, depth:0, activate:false }, //tensiometro1
        { val_max: 2000, val_min: 0, multiplier: -1, variable: 2, depth:0, activate:false },  //tensiometro2
        { val_max: 2000, val_min: 0, multiplier: -1, variable: 3, depth:0, activate:false },  //tensiometro3
        { val_max: 2000, val_min: 0, multiplier: -1, variable: 4, depth:0, activate:false },  //tensiometro4
        { val_max: 100000000, val_min: 0, multiplier: -3, variable: 5,activate:false }, //conductividad 2
        { val_max: 2000, val_min: -400, multiplier: -1, variable: 6,activate:false }, //temperatura
        { val_max: 1000, val_min: 0, multiplier: -1, variable: 7,activate:false }, //contenido volumetrico
        { val_max: 600, val_min: -400, multiplier: -1, variable: 8,activate:false }, //temperatura 2
        { val_max: 1000, val_min: 0, multiplier: -1, variable: 9,activate:false }, //humedad
        { val_max: 9999, val_min: 0, multiplier: 0, variable: 10,activate:false }, //lluvia
        { val_max: 1000, val_min: 0, multiplier: -1, variable: 11,activate:false }, //Velocidad del viento
        { val_max: 300, val_min: 0, multiplier: 0, variable: 12,activate:false }, //luz
        { val_max: 1100, val_min: 300, multiplier: 0, variable: 13,activate:false }, //presion del aire
        { val_max: 80, val_min: 0, multiplier: -1, variable: 14,activate:false }, //bateria
       ]
      Device.create(data)
      .then(result => {
        if (data.type_id == '2') {
          for (var i = 0; i < datajson.length; i++) {
            datajson[i].device_id = result.id;
          }
          Detail.bulkCreate(datajson).then(resultDetail=>{
            console.log('Insert details and it is valvula')
          })  
        }
        
         template.view.flash.set('success', 'El dispositivo fue creado', template.view);
              return res.redirect('/device/list');
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/device/add');
      });

  }

  async edit(req, res, next) {
      let types = await Type.findAll({
        order: [['name', 'DESC']]
      });
      let farmers = await User.findAll({
        where:{role_id:3},
      });
      
      Device.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Type },{model:User}]})
      .then(result => {
         template.data = {device: result, types: types,farmers:farmers};
         template.layout.view = 'device/edit';
        res.render(template.layout.default, template);
      });
  }

  editsave(req, res, next) {

      let $data = req.body.data;
      $data.active = ('active' in $data) ? true : false;
      $data.id = req.params.id;

      Device.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios al dispositivo', template.view);
          return res.redirect('/device/list');
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/device/edit/' + req.params.id);
      });

  }
  async editValve(req, res, next) {
      let types = await Type.findAll({
        order: [['name', 'DESC']]
      });
      let farmers = await User.findAll({
        where:{role_id:3},
      });
      let groups = await Group.findAll({
        where:{farmer_id: req.user.id}
      })
      Device.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Type },{model:User},{model:Group}]})
      .then(result => {
         template.data = {device:result, types:types, farmers:farmers, groups:groups};
         template.layout.view = 'device/editValve';
        res.render(template.layout.default, template);
      });
  }
  editvalvesave(req, res, next) {
      let $data = req.body.data;
      $data.id = req.params.id;

      Device.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios al dispositivo', template.view);
          return res.redirect('/device/valve');
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/device/editValve/' + req.params.id);
      });

  }
  async editgroup(req, res, next){
    let irrigations = await Irrigation.findAll({
      where:{farmer_id: req.user.id}
    })
    Group.findOne({where: {id:parseInt(req.params.id)}, include:[{model:User}]})
    .then(result=>{
      template.data = {device:result, irrigations:irrigations};
      template.layout.view = 'device/editgroup';
      res.render(template.layout.default, template);
    });


  }
  editgroupsave(req, res, next) {
      let $data = req.body.data;
      $data.id = req.params.id;

      Group.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios al grupo', template.view);
          return res.redirect('/device/groups');
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/device/editgroup/' + req.params.id);
      });

  }
  delete(req, res, next) {
      Device.destroy({where: {id: req.params.id}})
      .then(result =>{
          Detail.destroy({where:{device_id:req.params.id}}).then(result_s=>{
            template.view.flash.set('success', 'Se eliminó el dispositivo', template.view);
            return res.redirect('/device/list');  
          })
      });

  }
  validate(req, res, next) {

      template.layout.view = 'device/validate';
      res.render(template.layout.default, template);

  }

  async validatesave(req, res, next) {

    let $data = {
      assign: true,
      farmer_id: req.user.id
    }
    let $params = {
      where: {code: req.body.data.code}
    }

    let $device = await Device.findOne($params);

    if($device){
    console.log($device);

      if(!$device.assign){

        Device.update($data, $params)
        .then( (result, result2) => {
          
          template.view.flash.set('success', 'Se agrego el dispositivo correctamente', template.view);
          if($device.type_id == 1){
            return res.redirect('/device/valve');  
          }
          return res.redirect('/device/sensor');
        })
      }else {
        template.view.flash.set('error', 'El codigo del dispositivo ya fue validado', template.view);
        return res.redirect('/device/validate');
      }

    } else {
      template.view.flash.set('error', 'El codigo del dispositivo no existe', template.view);
      return res.redirect('/device/validate');
    }
  }

  sensor(req, res, next) {
    
    let $params = {
       where : {
          farmer_id: req.user.id,
          type_id: 2
       },
       include: [{model:Reading}]
    }
    Device.findAll($params).then(result=>{
      template.data = {devices: result};
      template.layout.view = 'device/sensor';
      res.render(template.layout.default, template);
    });
      
  }
  valves(req, res, next) {
    let $params = {
       where : {
          farmer_id: req.user.id,
          type_id: 1
       },
       include: [{ model: Group },{model:Reading}]
    }
    
    Device.findAll($params).then(result=>{
      template.data = {devices: result};
      template.layout.view = 'device/valve';
      res.render(template.layout.default, template);
    });
      
  }

  gateway(req, res, next) {
    let $params = {
       where : {
          farmer_id: req.user.id,
          type_id: 3
       },
       include: [{model:Reading}]
    }
    Device.findAll($params).then(result=>{
      template.data = {devices: result};
      template.layout.view = 'device/gateway';
      res.render(template.layout.default, template);
    });
      
  }
  async groups(req, res, next){

    let irrigations = await Irrigation.findAll({
      where: {farmer_id:req.user.id},
      include: [{model:Group}]
    })
    
    Group.findAll({
      where: {farmer_id: req.user.id},
      include: [{model:Device}]
    }).then(result=>{
      
      template.data = {groups: result, irrigations:irrigations};
      template.layout.view = 'device/groups';
      res.render(template.layout.default, template);  
    })
    
  }

  addgroup(req, res, next){
    let data = req.body.data;
    data.farmer_id = req.user.id;
    Group.create(data).then(result=>{
      template.view.flash.set('success','Se creo un nuevo grupo', template.view);
      return res.redirect('/device/groups');
    }).catch(Sequelize.ValidationError, function (msg) {          
        template.view.flash.set('error', msg.errors[0].message, template.view);
        return res.redirect('/device/groups');
    });
  }
  async devicebygroup(req, res, next){
    let idgroup = req.query.idgroup;
    let devices = await Device.findAll({
      where: {group_id:idgroup}
    })
    res.json({"devices": devices});
    
  }

  deletegroup(req, res, next){
    Group.destroy({where: {id: req.params.id}})
      .then(result =>{
          template.view.flash.set('success', 'Se eliminó el grupo', template.view);
         return res.redirect('/device/groups');
      });
  }
  dropgroup(req, res, next){
    let id = req.params.id;
    let data = {group_id:null};
    Device.update(data, {where: {id: id} } )
    .then(result => {template.view.flash.set('success', 'Se elimino el dispositivo del grupo', template.view);
        return res.redirect('/device/groups');
    }).catch(Sequelize.ValidationError, function (msg) {          
        template.view.flash.set('error', msg.errors[0].message, template.view);
        return res.redirect('/device/groups/');
    });
  }

  async groupbyirrigation(req, res, next){
    let idgroup = req.query.idgroup;
    let groups = await Group.findAll({
      where: {irrigation_id:idgroup}
    })
    res.json({"groups": groups});
    
  }
  deleteirrigation(req, res, next){
    Irrigation.destroy({where: {id: req.params.id}})
      .then(result =>{
          template.view.flash.set('success', 'Se eliminó el esquema de irrigación', template.view);
         return res.redirect('/device/groups');
      });
  }
  dropirrigation(req, res, next){
    let id = req.params.id;
    let data = {irrigation_id:null};
    Group.update(data, {where: {id: id} } )
    .then(result => {template.view.flash.set('success', 'Se elimino el grupo del esquema de irrigación', template.view);
        return res.redirect('/device/groups');
    }).catch(Sequelize.ValidationError, function (msg) {          
        template.view.flash.set('error', msg.errors[0].message, template.view);
        return res.redirect('/device/groups');
    });
  }
  async devicesAddRemote(req, res, next){
    
    let idC = req.body.coordinate.coordinateid.toString()
    let idD = req.body.device.devid.toString()

    
    var data = req.body;
    Device.findAll({
      where:{
        code: {
          [Sequelize.Op.in]:[idC,idD]  
        } 
      },
      order:[['type_id','DESC']]
    }).then(result=>{
      if (result.length == 2) {
        let data_gateway = req.body.coordinate;
        let date = moment(data.coordinate.date);

        let gatewayDevice_id = result[0].id;

        data_gateway.date_read = date;

        data_gateway.device_id = gatewayDevice_id;

        var sensorOrvalve = req.body.device.code;
        
        Reading.create(data_gateway).then(resultGateway=>{
          let gateway_id = resultGateway.id;
          let sensOrvalv_id = result[1].id;
          let data_sensOrvalv = req.body.device;
          //let code = req.bod.device.code;

          data_sensOrvalv.date_read = date;

          data_sensOrvalv.gateway_id = gateway_id;
          data_sensOrvalv.device_id = sensOrvalv_id;
          Reading.create(data_sensOrvalv).then(resultDevice=>{
            console.log('insert device')
            let dataresp = {
              "gatewayid": resultGateway.device_id,
              "deviceid": resultDevice.device_id
             }
            res.status(200).json({
              "status": "success",
              "code": 200,
              "message": "save in data base",
              "data":  dataresp
            });
          }).catch(function(err){
            res.status(500).json({
              "status": "error",
              "code": 500,
              "message": err,
              "data": null
            });
          })
        }).catch(function(err){
          console.log(err)
          res.status(500).json({
            "status": "error",
            "code": 500,
            "message": err,
            "data": null
            });
        })
      }
      else{
        console.log('error dont register the coordinate or device')
        res.status(500).json({
          "status": "error",
          "code": 500,
          "message": "the gateway or device is not register",
          "data": null
        });
      }
    })
 
  }
  async devicesAddLocal(req, res, next){
    
    let idC = req.body.coordinate.coordinateid.toString()
    let idD = req.body.device.devid.toString()
    
    var data = req.body;
    Device.findAll({
      where:{
        code: {
          [Sequelize.Op.in]:[idC,idD]  
        } 
      },
      order:[['type_id','DESC']]
    }).then(result=>{
      if (result.length == 2) {
        let data_gateway = req.body.coordinate;
        let date = moment(data.coordinate.date);

        let gatewayDevice_id = result[0].id;

        data_gateway.date_read = date;

        data_gateway.device_id = gatewayDevice_id;

        var sensorOrvalve = req.body.device.code;
        
        Reading.create(data_gateway).then(resultGateway=>{
          let gateway_id = resultGateway.id;
          let sensOrvalv_id = result[1].id;
          let data_sensOrvalv = req.body.device;
          //let code = req.bod.device.code;

          data_sensOrvalv.date_read = date;

          data_sensOrvalv.gateway_id = gateway_id;
          data_sensOrvalv.device_id = sensOrvalv_id;
          Reading.create(data_sensOrvalv).then(resultDevice=>{
            console.log('insert device in local')
            request.post({
              url: extra_config.api_remote.url+extra_config.api_remote.api.device_add_remote, 
              form: req.body
              }, function(err,httpResponse,body){
               console.log('save data in server')
             })

             let dataresp = {
              "gateway": resultGateway.device_id,
              "device": resultDevice.device_id
             }
            res.status(200).json({
              "status": "success",
              "code": 200,
              "message": "save in data base",
              "data":  dataresp
            });
          }).catch(function(err){
            res.status(500).json({
              "status": "error",
              "code": 500,
              "message": err,
              "data": null
            });
          })
        }).catch(function(err){
          res.status(500).json({
            "status": "error",
            "code": 500,
            "message": err,
            "data": null
            });
        })

      }
      else{
        console.log('error dont register the coordinate or device')
        res.status(500).json({
          "status": "error",
          "code": 500,
          "message": "the gateway or device is not register",
          "data": null
        });
      }
    })
 
  }
  configuration(req, res, next){
    let variables = ["Tensiometro 1","Tensiometro 2","Tensiometro 3","Tensiometro 4",
    "Conductividad Eléctrica", "Temperatura", "Contenido Volumétrico",
    "Temperatura", "Humedad", "Lluvia", "Velocidad del Viento", "Luz", "Presión del Aire", "Bateria"]

    Device.findOne({
      where:{id:req.params.id},
      include: [{ model: Detail }],
      order:[[Detail,'variable','asc']]
    }).then(result=>{
      
      template.data = {device:result, variables:variables};
      template.layout.view = 'device/configuration';
      res.render(template.layout.default, template);  
    })
    
  }
  getvalues(req, res, next){
    Detail.findOne({
      where:{id:req.params.id}
    }).then(result=>{
      res.json({'data':result})
    })


  }
  savegetvalues(req, res, next){
    let data = req.body.data;
    let id = data.valid;
    let device_idget = data.device_id;
    console.log(data)
    
    Detail.update(data, {where: {id:id} } )
    .then(result => {
        template.view.flash.set('success', 'Se realizó los cambios a los valores', template.view);
        return res.redirect('/device/configuration/' + device_idget);
    }).catch(Sequelize.ValidationError, function (msg) {          
        template.view.flash.set('error', msg.errors[0].message, template.view);
        return res.redirect('/device/configuration/' + device_idget);
    });
  }
  
  detailActive(req, res, next){
    Detail.findOne({
          where: { id: parseInt(req.params.id) }
      })
      .then(result => {
          result.activate = !result.activate;
          result.save()
          .then(result => {
              template.view.flash.set('success', 'Se cambio el estado de la variable', template.view); 
              return res.redirect(req.get('referer'));
          });
      });
  }

  async graph(req, res, next){
    
    let sevenDaysAgo = moment(new Date((new Date()).getTime() - (7 * 24 * 60 * 60 * 1000))).format('Y-MM-DD');
    let listDevice = await Reading.findAll({
      where:{
        device_id: req.params.id,
        date_read: {[Sequelize.Op.gte]: sevenDaysAgo + ' 00:00:00'+ '.582Z'},
      },
      order: [['date_read', 'ASC']]
    })
   
    let listValves = await Device.findAll({
      where:{
        farmer_id: req.user.id,
        type_id: 1
      },
      include:[{model: Reading}]
    });
  
    Device.findOne({
      where: {id: parseInt(req.params.id)}, 
      include: [{ model: Detail }],
      order:[[Detail,'variable','asc']]
    }).then(result => {
      template.data = {device:result, listDevice:listDevice, listValves:listValves};
      template.layout.view = 'device/graph';
      res.render(template.layout.graph, template);
    });
    
  }
  async graph2(req, res, next){
    
    let sevenDaysAgo = moment(new Date((new Date()).getTime() - (7 * 24 * 60 * 60 * 1000))).format('Y-MM-DD');
    let listDevice = await Reading.findAll({
      where:{
        device_id: req.params.id,
        date_read: {[Sequelize.Op.gte]: sevenDaysAgo + ' 00:00:00'+ '.582Z'},
      },
      order: [['date_read', 'ASC']]
    })
    let listValves = await Device.findAll({
      where:{
        farmer_id: req.user.id,
        type_id: 1
      },
      include:[{model: Reading}]
    });
  
    Device.findOne({
      where: {id: parseInt(req.params.id)}, 
      include: [{ model: Detail }],
      order:[[Detail,'variable','asc']]
    }).then(result => {
      template.data = {device:result, listDevice:listDevice, listValves:listValves};
      template.layout.view = 'device/graph2';
      res.render(template.layout.graph, template);
    });
    
  }

  async graph_from_device1(req, res, next){
    let nowDay = moment().format('Y-MM-DD')
    let sevenDaysAgo = moment(new Date((new Date()).getTime() - (7 * 24 * 60 * 60 * 1000))).format('Y-MM-DD');

    Device.findOne({
      where: {device_idmdb: req.params.id},
      include: [{ model: Detail }],
      order:[[Detail,'variable','asc']]
    }).then(async result=>{
      let listValves = await Device.findAll({
        where:{
          farmer_id: result.id,
          type_id: 1
        },
        include:[{model: Reading}]
      });

      Reading.findAll({
        where:{
          device_id: result.id,
          date_read: {[Sequelize.Op.gte]: sevenDaysAgo + ' 00:00:00'+ '.582Z'},
        },
        order: [['date_read', 'ASC']]
      }).then(listDevice =>{
        template.data = {device:result, listDevice:listDevice, listValves:listValves};
        template.layout.view = 'device/graph';
        res.render(template.layout.graph, template);  
      })
      
    })
  }
  async graph_from_device2(req, res, next){
    let nowDay = moment().format('Y-MM-DD')
    let sevenDaysAgo = moment(new Date((new Date()).getTime() - (7 * 24 * 60 * 60 * 1000))).format('Y-MM-DD');

    Device.findOne({
      where: {device_idmdb: req.params.id},
      include: [{ model: Detail }],
      order:[[Detail,'variable','asc']]
    }).then(async result=>{
      let listValves = await Device.findAll({
        where:{
          farmer_id: result.id,
          type_id: 1
        },
        include:[{model: Reading}]
      });

      Reading.findAll({
        where:{
          device_id: result.id,
          date_read: {[Sequelize.Op.gte]: sevenDaysAgo + ' 00:00:00'+ '.582Z'},
        },
        order: [['date_read', 'ASC']]
      }).then(listDevice =>{
        template.data = {device:result, listDevice:listDevice, listValves:listValves};
        template.layout.view = 'device/graph2';
        res.render(template.layout.graph, template);  
      })
      
    })
  }
  async getdatafilters(req, res, next){
    var data1_start_date = req.query.datajson.data_st1_date;
    var data1_start_time = req.query.datajson.data_st1_time;
    var data1_end_date = req.query.datajson.data_ed1_date;
    var data1_end_time = req.query.datajson.data_ed1_time;
    var deviceId = parseInt(req.query.datajson.deviceid);
    
    var start = moment(data1_start_date +' '+ data1_start_time, "YYYY-M-D H:m A").format("YYYY-MM-DD HH:mm:ss") + '.582Z';
    var end = moment(data1_end_date +' '+ data1_end_time, "YYYY-M-D H:m A").format("YYYY-MM-DD HH:mm:ss") + '.582Z';
    
    let listDevice = await Reading.findAll({
        where:{
          device_id: deviceId,
          date_read : {[Sequelize.Op.gte]: start ,[Sequelize.Op.lte]: end}
        },
        order:[['date_read','ASC']]
      })
    res.json({'listDevice':listDevice}) ;
  }

  save3values(req, res, next){
    let $data = req.body.data;
    $data.id = req.params.id;

    Device.update($data, {where: {id: req.params.id} } )
    .then(result => {
        template.view.flash.set('success', 'Se realizó los cambios a las variables', template.view);
        return res.redirect('/device/configuration/' + req.params.id);
    }).catch(Sequelize.ValidationError, function (msg) {          
        template.view.flash.set('error', msg.errors[0].message, template.view);
        return res.redirect('/device/configuration/' + req.params.id);
    });
  }
  valve(req, res, next){
    let id = req.params.id;
    
    Reading.findAll({
      order: [['date_read','DESC']],
      where: {
        device_id: req.params.id,
        state:true

      }
    }).then(result=>{
      res.json({'valves':result});
    }).catch(err=>{
      res.status(500).json({"message": err});
    })
  }

  async download(req, res, next){
    let device_id = req.params.id;
    let body = req.body.data;
    let date_start = moment(body.date_start +' '+ body.time_start, "YYYY-M-D H:m A").format("YYYY-MM-DD HH:mm:ss") + '.582Z';
    let date_end = moment(body.date_start +' '+ body.time_start, "YYYY-M-D H:m A").format("YYYY-MM-DD HH:mm:ss") + '.582Z';

    let datas = await Reading.findAll({
      where: {
        device_id: device_id,
        date_read: {[Sequelize.Op.gte]: date_start, [Sequelize.Op.gte]: date_end}
      }
    })
    let device = await Device.findById(device_id,{include:[{model:Type}]})
    
    var datafill = [];
    var struct;
    var i=0;

    for (const data of datas){
      struct = {
        _id: data.id,
        device: '',
        type: '',
        rssitx: data.rssitx,
        rssirx: data.rssirx,
        battery: data.battery,
        tensiometer1: data.tensiometer1,
        tensiometer2: data.tensiometer2,
        tensiometer3: data.tensiometer3,
        tensiometer4: data.tensiometer4,
        conductivity: data.conductivity,
        temperature: data.temperature,
        volume_content: data.volume_content,
        temperature_station: data.temperature_station,
        humidity: data.humidity,
        rain: data.rain,
        speed_wind: data.speed_wind,
        light: data.light,
        pressure_air: data.pressure_air,
        battery2: data.battery2,
        date: moment(data.date_read).format('YYYY-MM-DD'),
        time: moment(data.date_read).format('HH:mm:ss')
      }
      if (i==0) {
        struct.device = device.code,
        struct.type = device.type.name
      }
      i++;
      datafill.push(struct)
    }
    
    // define the order of the columns and their labels here
    var columns = [
    {
      prop: 'device',
      label: 'Dispositivo'
    },{
      prop: 'type',
      label: 'Tipo'
    },{
      prop: 'rssitx',
      label: 'RSS Tx'
    }, {
      prop: 'rssirx',
      label: 'RSS Rx'
    }, {
      prop: 'battery',
      label: 'Bateria'
    }, {
      prop: 'tensiometer1',
      label: 'Tensiometro 1'
    },{
      prop: 'tensiometer2',
      label: 'Tensiometro 2'
    },{
      prop: 'tensiometer3',
      label: 'Tensiometro 3'
    },{
      prop: 'tensiometer4',
      label: 'Tensiometro 4'
    },{
      prop: 'conductivity',
      label: 'Conductividad Eléctrica'
    },{
      prop: 'temperature',
      label: 'Temperatura'
    },{
      prop: 'volume_content',
      label: 'Contenido Volumétrico'
    },{
      prop: 'temperature_station',
      label: 'Temperatura'
    },{
      prop: 'humidity',
      label: 'Humedad'
    },{
      prop: 'rain',
      label: 'Lluvia'
    },{
      prop: 'speed_wind',
      label: 'Velocidad del Viento'
    },{
      prop: 'light',
      label: 'Luz'
    },{
      prop: 'pressure_air',
      label: 'Presión de aire'
    },{
      prop: 'battery2',
      label: 'Bateria'
    },{
      prop: 'date',
      label: 'Fecha'
    },{
      prop: 'time',
      label: 'Hora'
    }

    ]
    res.csv('reporte', datafill, columns);


  }


}


