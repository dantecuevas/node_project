import Controller from './Controller'

import Sequelize from 'sequelize'
import moment from 'moment'
import GroupModel from '../models/group';
import IrrigationModel from '../models/irrigation';
import EventModel from '../models/event';

var Group, Irrigation, Event;

export default (Src, template, middleware) => class IrrigationController extends Controller {

   constructor(){
      super()
      Group = GroupModel(Src.DB.sequelize);
      Irrigation = IrrigationModel(Src.DB.sequelize);
      Event = EventModel(Src.DB.sequelize);
      Group.belongsTo(Irrigation, {foreignKey:'irrigation_id'});
      Event.belongsTo(Group, {foreignKey:'group_id'});
      Group.hasMany(Event, {foreignKey:'group_id'});

      
   }

   async view(req, res, next) {
      let irrigations = await Irrigation.findAll({
        where:{farmer_id: req.user.id}
      })
      let groups = await Group.findAll({
      	where: {farmer_id: req.user.id}
      })
      template.data = {groups:groups, irrigations:irrigations}
      template.layout.view = 'calendar/view'
      res.render(template.layout.default, template);

   }
   addirrigation(req, res, next){
    let data = req.body.data;
    data.farmer_id = req.user.id;
    Irrigation.create(data).then(result=>{
      template.view.flash.set('success','Se creo un nuevo grupo', template.view);
      return res.redirect('/device/groups');
    }).catch(Sequelize.ValidationError, function (msg) {          
        template.view.flash.set('error', msg.errors[0].message, template.view);
        return res.redirect('/device/groups');
    });
  }

  async newScheduleIrrigation(req, res, next){
    let irrigations = await Irrigation.findAll({
        where:{farmer_id: req.user.id}
      })
      let groups = await Group.findAll({
        where: {farmer_id: req.user.id}
      })
      template.data = {groups:groups, irrigations:irrigations}
    template.layout.view = 'calendar/newIrrigation';
    res.render(template.layout.default, template);
  }
  async eventsave(req, res, next){
    var data = req.body.data;
    var idgroup = req.body.data1;
    for(let ele of data){
      if (ele.day) {
        ele.group_id = idgroup.group_id;
        Event.create(ele)
      }
    }
   
    template.view.flash.set('success','Se creo un nuevo evento', template.view);
      return res.redirect('/calendar/view');
   

  }
  async getgroups(req, res, next){


    /*let groups = await Group.findAll({
      where:{irrigation_id:req.body.irrigation_id},
      include: [{model: Event}],
      order: [['name','ASC']]
    });*/
    let groupirr = await Group.findAll({
      where:{irrigation_id:req.body.irrigation_id},
      include: [{model: Irrigation}],
      
    });
    console.log('----------------------------------------');
    console.log(groupirr);
    console.log(groupirr[0].irrigation)
    /*let irrigation = await Irrigation.findOne({
      where:{id:req.body.irrigation_id},
      //include: [{model: Group}]
    });/**/
    console.log('**********************')
    console.log(irrigation)  
    return res.send({irrigation:irrigation, groups: groups});
  }
  async getgroupsdata(req, res, next){

    let group = await Group.findOne({
      where:{id:req.body.group_id}
    });
    
    return res.send({group: group});
  }

}
