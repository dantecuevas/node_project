import Controller from './Controller'

import Sequelize from 'sequelize'
import moment from 'moment'
import Polygon from "../models/mongo/polygon";
import Fields from "../models/mongo/field";
import Point from "../models/mongo/point";
import DevicePoint from "../models/mongo/device";
import DeviceModel from '../models/device'
import ReadingModel from '../models/reading';

let Device, Reading;
var op, oplast;


export default (Src, template, middleware) => class UserController extends Controller {

   constructor(){
    super()
    Device = DeviceModel(Src.DB.sequelize);
    Reading = ReadingModel(Src.DB.sequelize);
    Device.hasMany(Reading, {foreignKey: 'device_id'});
   }

   async savePolygon(req, res, next) {
      let data = req.body.data;
      let field_id = req.body.fieldId;
      data.field_id = field_id;
      let newdataPoly = new Polygon(data);
      newdataPoly.id = newdataPoly._id;
      var savepoly = await newdataPoly.save();
      var field = await Fields.findOne({_id:field_id});
      var polygons = await Polygon.find({field_id:field_id});
      
      if (savepoly) {
         
         res.status(200).json({field:field,polygons: polygons});   
      }
      else{
         return res.sendStatus(500).json({err:"error save or read"});
      }
      
   }

   async view(req, res, next) {
      var fields = await Fields.find({farmer_id:req.user.id});
      if (fields.length == 0) {
         template.view.flash.set('warning', 'Agregue un campo antes de ingresar a "Suelo"', template.view);
         template.layout.view = 'field/add'
         res.render(template.layout.default, template);
      }
      else{
         oplast = op;
         template.data = {fields:fields, opp :op };
         template.layout.view = 'farm/view'
         res.render(template.layout.default, template);
      }
   }
   
   async changefield(req, res, next){
      let id = req.query.field_id;
      op = req.query.op;
      var field = await Fields.findOne({_id:id});
      var polygons = await Polygon.find({field_id:id});
      var devices = [];
      var tmp
      var points
      for (const poly of polygons){
         points = await DevicePoint.find({polygon_id:poly._id});
         tmp = devices.concat(points);
         devices = tmp;
      }
      res.json({'field':field,'polygons':polygons, 'devices':devices, 'op':op});
   }

   async generatepoints(req, res, next) {
      var idPoly = req.query.id;
      var coorUbic = {lat:parseFloat(req.query.lat),lng:parseFloat(req.query.lng)}
      var polygon = await Polygon.findOne({_id:idPoly});
      var points = await Point.find({polygon_id:idPoly});
      
      template.view.flash.set('warning', 'Se ha seleccionado este cultivo. Ahora puede añadir caracteristicas'+
      ' del suelo. Colocar puntos por medio de un click dentro del cultivo (poligono).'+
      'Luego hacer click en cada punto para ver las características del suelo.', template.view);
      template.data = {coord: coorUbic, polygon: polygon, points:points};
      template.layout.view = 'farm/generatepoints';
      res.render(template.layout.default, template);
      
   }
   async generatedevices(req, res, next) {
      var idPoly = req.query.id;
      var coorUbic = {lat:parseFloat(req.query.lat),lng:parseFloat(req.query.lng)}
      var polygon = await Polygon.findOne({_id:idPoly});
      var points = await DevicePoint.find({polygon_id:idPoly});
      
      template.view.flash.set('warning', 'Se ha seleccionado este cultivo. Ahora puede añadir los dispositivos.'+
      ' Seleccione el tipo de dispositivo, luego selecione un dispositivo y por medio de un click dentro del cultivo'+
      ' (poligono) puede agregar los dispositivos.'+
      'Luego hacer click en cada uno para ver los detalles', template.view);
      template.data = {coord: coorUbic, polygon: polygon, points:points};
      template.layout.view = 'farm/generatedevices';
      res.render(template.layout.default, template);
      
   }
   async savePoint(req, res, next) {
      let data = JSON.parse(req.body.coordinates);
      let polygonId = req.body.polygonId;
      let point = {
         location: {
            type: 'Point',
            coordinates: [data.lat, data.lng]
         },
         polygon_id: polygonId
      }
      let newpoint = new Point(point);
      var newpo = await newpoint.save();
      var polygon = await Polygon.findOne({_id:polygonId});
      Point.find({polygon_id:polygonId},(err,data)=>{
         if(!err){
            res.status(200).json({points: data, polygon:polygon});
         } else {
            return res.sendStatus(500).json(err);
         } 
      })
   }
   async saveDevice(req, res, next) {
      let data = JSON.parse(req.body.coordinates);
      let polygonId = req.body.polygonId;
      let typedevice = req.body.typeofdevice
      let device_id = req.body.device_id;
      let point = {
         location: {
            type: 'Point',
            coordinates: [data.lat, data.lng]
         },
         polygon_id: polygonId,
         typedevice: typedevice,
         device_id: device_id
      }
   
      let newpoint = new DevicePoint(point);
      var newpo = await newpoint.save();
      var polygon = await Polygon.findOne({_id:polygonId});
      
      let dataupdate = {device_idmdb:(newpo._id).toString()};
      let update = await Device.update(dataupdate, {where: {code: device_id} } )
      
      DevicePoint.find({polygon_id:polygonId},(err,data)=>{
         if(!err){
            res.status(200).json({points: data, polygon:polygon});
         } else {
            return res.sendStatus(500).json(err);
         } 
      })



   }
   deletePoint(req, res, next) {
      let pointId = req.body.id;
      Point.findByIdAndRemove(pointId,(err,doc)=>{
         if(!err){
            res.status(200).json({message: doc});
         } else {
            return res.sendStatus(500).json(err);
         } 
      })
   }
   async deleteDevice(req, res, next) {
      let pointId = req.body.id;
      let dataupdate = {device_idmdb:null}
      let update = await Device.update(dataupdate, {where: {device_idmdb: pointId.toString()} } )
      DevicePoint.findByIdAndRemove(pointId,(err,doc)=>{
         if(!err){
            res.status(200).json({message: doc});
         } else {
            return res.sendStatus(500).json(err);
         } 
      })
   }
   detailPoint(req, res, next) {
      let idPoint = req.params.id;
      Point.findOne({_id:idPoint},(err,result)=>{
         if (!err) {
            res.json({'point':result})
         }
         else{
            res.send({result: 'error', err: err});
         }
      })
   }
   detailDevice(req, res, next) {
      let params = {
         where : {
          device_idmdb: (req.params.id).toString(),
         },
         include: [{model:Reading}]
      }
         
      Device.findOne(params).then(result=>{
         console.log(result)
         res.json({'point':result})
      });
      
   }
   detailPointEdit(req, res, next) {
      let data = JSON.parse(req.body.datasend);
      let datasend = {
         id:data.polygon._id,
         coor:data.coord
      }
      let idPoint = req.body.idPoint;
      Point.findOne({_id:idPoint},(err,result)=>{
         template.data = {details: result, pointId:idPoint, data:datasend};
         template.layout.view = 'farm/detailsPoint';
         res.render(template.layout.default, template);   
      })
   }
   async detailPointSave(req, res, next) {
      let pointId = req.body.point_id;
      let data = req.body.data;
      var pointsave = await Point.findOneAndUpdate({_id:pointId},{$set:{detail:data}});
      var polyId = pointsave.polygon_id;
      var coord = pointsave.location.coordinates;
      let coorUbi = {lat:coord[0],lng:coord[1]};
      var polygon = await Polygon.findOne({_id:polyId});
      var points = await Point.find({polygon_id:polyId});
      template.view.flash.set('success', 'Se guardo los detalles', template.view);
      template.data = {coord: coorUbi, polygon: polygon, points:points};
      template.layout.view = 'farm/generatepoints';
      res.render(template.layout.default, template);
      return
      
   }

   async deletePolygonAndPoint(req, res, next) {
      
      let markers_ids = req.body.markers_ids;
      let polyId = req.body.IdPoly;
      var pointIds = [];

      let poly = await Polygon.findOneAndRemove({_id:polyId}) 
      let point = await Point.remove({_id:{$in:markers_ids}},(err,data)=>{
         if(!err){
            res.send({msg: "deleted"});
         } else {
            res.send({result: 'error', err: err});
         }
      })
   }
   async getDevices (req, res, next){
      let user = req.user.id;
      let type = req.body.typeofdevice;
      let devices = await Device.findAll({
      where:{
            [Sequelize.Op.and]:[
               {farmer_id: user},
               {type_id:type}
            ],
            device_idmdb:null
         },
         order: [['created','ASC']]
      });
      return res.send({devices: devices});
   }

   async savefield_detail(req, res, next){
      var fields = await Fields.find({farmer_id:req.user.id});
      let data = req.body.data;

      Polygon.update(
      {_id:data._id,},
      {
         $set:{
            typefield: data.typefield,
            dateS: data.dateS,
            dateC: data.dateC
         }
      }).then(resul=>{
         template.view.flash.set('success', 'Se guardo el cultivo', template.view);
         template.data = {fields:fields, opp :op};
         template.layout.view = 'farm/view'
         res.render(template.layout.default, template);
      }).catch(err=>{
         template.view.flash.set('error', 'Error en guardar los detalles del cultivo', template.view);
         template.data = {fields:fields, opp :op};
         template.layout.view = 'farm/view'
         res.render(template.layout.default, template);
      })
    
   }

   getdatapolygon(req, res, next){
      let idPoint = req.params.id;
      
      Polygon.findOne({_id:idPoint}).then(result=>{
         res.json({'point':result})
      }).catch(err=>{
         res.json({'point':err})
      })
   }
  

}



