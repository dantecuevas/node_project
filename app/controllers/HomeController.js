import Controller from './Controller'

import Sequelize from 'sequelize'
import moment from 'moment'

export default (Src, template, middleware) => class HomeController extends Controller{

   constructor() {
      super()
   }

   index (req, res, next) {
      template.layout.default = 'layout/default_landing';
      template.layout.view = 'landing/main';
      res.render(template.layout.default, template);
   }
   
   admin (req, res, next) {
      template.layout.view = 'admin/home'
      res.render(template.layout.default, template);
   }

   farmer (req, res, next) {
      template.layout.view = 'farmer/home'
      res.render(template.layout.default, template);
   }

}
