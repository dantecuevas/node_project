import Controller from './Controller'

import sha1 from 'sha1';
import Sequelize from 'sequelize';
import moment from 'moment'
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import generator from 'generate-password';
import config from '../configs/config';
import UserModel from "../models/user";
import RoleModel from "../models/role";
import CountryModel from "../models/country";
import StateModel from "../models/state";
import CityModel from "../models/city";

let User, Role, Country, State, City;


var transporter = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    secure: config.mail.secure, // use SSL
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
});
export default (Src, template, middleware) => class UserController extends Controller {
//export default (Src, template, middleware) => class UserController{

  constructor(){
    super()
    User = UserModel(Src.DB.sequelize);
    Role = RoleModel(Src.DB.sequelize);
    Country = CountryModel(Src.DB.sequelize);
    State = StateModel(Src.DB.sequelize);
    City = CityModel(Src.DB.sequelize);
    User.belongsTo(Role, { foreignKey: 'role_id' });
    User.belongsTo(Country, {foreignKey: 'country_id'});
    State.belongsTo(Country, {foreignKey: 'country_id'});
    City.belongsTo(State, {foreignKey: 'state_id'});

  }


  login(req, res, next) {

    template.layout.default = 'layout/open';
    template.layout.view = 'user/login';
    res.render(template.layout.default, template);

  }

  loginvalidate (req, res, next) {
    middleware.passport.authenticate('local', (err, user, params) => {
      
      switch (req.accepts('html', 'json')) {
        case 'html':
          if (err) { return next(err); }
          
          if (!user) {
            if (params.msg==undefined) {
              template.view.flash.set('error', 'Ingrese usuario y contraseña', template.view);
              return res.redirect(303, '/login');  
            } 
            else{
              template.view.flash.set('error', params.msg, template.view);
              return res.redirect(303, '/login');  
            }
          }

          req.logIn(user, function (err) {
            if (err) { return next(err); }
            
            
            if(user.role.id == 1|| user.role.id == 2) {
              template.view.flash.set('success', 'Bienvenido '+ user.first_name   + '.', template.view);
              return res.redirect('/home/admin');
            }
            else{
              template.view.flash.set('success', 'Bienvenido '+ `${(user.first_name)?user.first_name:user.organization_name}`, template.view);
              return res.redirect('/home/farmer');
            }
          });
        break;
        default:
          res.status(406).send();

      }
    })(req, res, next);

  }

  async register (req, res, next) {
    let countries = await Country.findAll();
    template.data = {countries:countries};
    template.layout.default = 'layout/default_landing';
    template.layout.view = 'user/register';
    res.render(template.layout.default, template);

  }
  async registersave (req, res, next) {
    let data = req.body.data;
    let email = data.email;
    let password = data.password;
    data.token = sha1(crypto.createHash('md5').update(data.email).digest("hex"));
    data.role_id = 3;
    data.birthdate = (data.birthdate==undefined)? null:moment(data.birthdate,'DD-MM-YYYY').format('YYYY-MM-DD');
    data.is_company = true;
    let countries = await Country.findAll();
    
    User.create(data).then(user=>{
      
      let $html = 'Su registro fue exitoso, estos son los datos de su cuenta:<br><br>';
        $html += '<b>Correo:</b> ' + email + '<br>';
        $html += '<b>Contraseña:</b> ' + password + '<br><br>';
        $html += 'Para validar su cuenta ingrese al siguiente <a href="' + template.view.endpoint('/activation') + '?t=' + user.id + '.' + data.token + '">Link</a>.';
        //$html += 'Para ingresar a la plataforma ingrese al siguiente <a href="' + template.helper.constants.SERVER_PUBLIC +  '">Link</a>.';
        $html += '<br><br>Este mensaje es autogenerado, por favor no responder a este correo.';

        let mailOptions = {
          from: `"Agrintell " ${config.mail.user}`, // sender address (who sends)
          to: data.email, // list of receivers (who receives)
          subject: `Infomación de Cuenta`, // Subject line
          text: `Los datos de su cuenta son, Email: ${email}, Contraseña: ${password}`, // plaintext body
          html: $html // html body
        };
        transporter.sendMail(mailOptions, function(error, info){
          if(error){
            template.view.flash.set('success', 'El registro fue exitos pero hubo problemas en el envió de su cuenta', template.view);
            template.data = {msj: "El registro fue exitoso pero hubo problemas en el envió de su cuenta"};
            template.layout.view = 'user/login'
            res.render(template.layout.default, template);
          } else {
            template.view.flash.set('success', 'El registro fue exitoso, se le envió un correo electrónico con los datos de su cuenta', template.view);
            template.data = {msj: "Su registro fue exitoso, se le envió un correo electrónico con los datos de su cuenta."};
            template.layout.default = 'layout/open';
            template.layout.view = 'user/login';
            res.render(template.layout.default, template);
          }
        });
    }).catch(Sequelize.ValidationError, function (msg) {
        
        
        template.view.flash.set('error', msg.errors[0].message, template.view);
        template.data = {msj: msg.errors[0].message,countries:countries};
        template.layout.default = 'layout/default_landing';
        template.layout.view = 'user/register';
        res.render(template.layout.default, template);
    });

  }

  activation (req, res, next) {

      let $pos = req.query.t.indexOf(".");
      let $id = req.query.t.substr(0, $pos);
     
      let $token = req.query.t.substr($pos + 1);

     

      User.findById($id)
        .then(result => {
            if (result) {
              if (result.token == $token && result.id == $id) {
          
                result.active = true;
                result.token = sha1(crypto.createHash('md5').update(result.email + Date.now()).digest("hex"));
                result.save()
                .then(result => {
                  template.view.flash.set('success', 'Su cuenta ha sido activada, puede iniciar sesión ', template.view);
                  template.data = {msj: "Su cuenta ha sido activada, puede iniciar sesión.",active:true};
                  template.layout.default = 'layout/open';
                  template.layout.view = 'user/login';
                  res.render(template.layout.default, template);
                });
              } else {
                  
                  template.view.flash.set('error', 'Su cuenta fue activada anteriormente o los campos de verificación no son correctos ', template.view);
                  template.data = {msj: "Su cuenta fue activada anteriormente o los campos de verificación no son correctos.", active: false};
                  template.layout.default = 'layout/open';
                  template.layout.view = 'user/login';
                  res.render(template.layout.default, template);
              }
            } else {
                template.view.flash.set('error', 'Su cuenta fue activada anteriormente o los campos de verificación no son correctos ', template.view);
                  template.data = {msj: "Su cuenta fue activada anteriormente o los campos de verificación no son correctos.", active: false};
                  template.layout.default = 'layout/open';
                  template.layout.view = 'user/login';
                  res.render(template.layout.default, template);
            }
        });
  }

  logout (req, res, next) {

    req.logout();
    template.session = {};
    res.redirect('/login');

  }

  forgetpassword (req, res, next) {

    template.layout.default = 'layout/open';
    template.layout.view = 'user/forgetpassword';
    res.render(template.layout.default, template);

  }

  
   forgetpasswordsave (req, res, next) {
      
      User.findOne({where: {email: req.body.data.email} })
      .then(result => {
          if (result) {
              let $html = 'Recibimos tu pedido de que olvidaste tu contraseña.<br>'
              $html += 'Comienza a cambiarla por una nueva: ';
              $html += '<a href="' + template.view.endpoint('/changepassword') + '?t=' + result.id + '.' + result.email.substr(0, 1) + result.token + '">Cambiar Contraseña</a>.';

              let mailOptions = {
                from: `"Agrintell " ${config.mail.user}`, // sender address (who sends)
                to: result.email, // list of receivers (who receives)
                subject: `Recuperar Cuenta de Usuario`, // Subject line
                text: `Se recuperara la cuenta de ${result.email}`, // plaintext body
                html: $html // html body
              };
              transporter.sendMail(mailOptions, function(error, info){
                if(error){
                  template.view.flash.set('error', 'No se pudo enviar el correo electrónico, por favor intentelo nuevamente', template.view);
                  return res.redirect('/forgetpassword');
                } else {
                  template.view.flash.set('success', 'Se envió un correo electronico en el cual podrá recuperar su cuenta', template.view);
                  return res.redirect('/login');
                }
              });
          } else {
            
            template.view.flash.set('error', 'No existe una cuenta con este correo electrónico', template.view);
            return res.redirect('/forgetpassword');
          }
      });

   }

   changepassword (req, res, next) {

      template.layout.default = 'layout/open';
      template.layout.view = 'user/changepassword';
      res.render(template.layout.default, template);

   }

   changepasswordsave (req, res, next) {

      let $pos = req.query.t.indexOf(".");
      let $id = req.query.t.substr(0, $pos);
      let $firstChar = req.query.t.substr($pos + 1, 1);
      let $token = req.query.t.substr($pos + 2);
      let $data = req.body.data;
      if($data.password == $data.re_password) {
        User.findById($id)
          .then(result => {
              if (result) {
                if (result.email.substr(0, 1) == $firstChar) {
                  result.password = $data.password;
                  result.token = sha1(crypto.createHash('md5').update(result.email + Date.now()).digest("hex"));
                  result.save()
                  .then(result => {
                      template.view.flash.set('success', 'La contraseña de su cuenta ha sido cambiada, ya puede iniciar sesión.', template.view);
                      return res.redirect('/login');
                  });
                } else {
                    template.view.flash.set('error', 'Los campos de verificación no son los correctos.123', template.view);
                    return res.redirect('/login');
                }
              } 
          });
      } else {
          template.view.flash.set('error', 'La nueva contraseña no es igual a la confirmación', template.view);
          return res.redirect('/changepassword?t=' + req.query.t);
      }

   }

  async getstates (req, res, next) {
    
    
    let states = await State.findAll({
      where:{country_id:req.body.country},
      order: [['name','ASC']]
    });
    
    return res.send({states: states});
    

  }

  async getcities (req, res, next) {
    
    
    let cities = await City.findAll({
      where:{state_id:req.body.state},
      order: [['name','ASC']]
    });
    
    return res.send({cities: cities});
    

  }
}

