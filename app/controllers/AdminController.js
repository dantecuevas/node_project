import Controller from './Controller'

import sha1 from 'sha1';
import Sequelize from 'sequelize';
import moment from 'moment';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import config from '../configs/config';
import generator from 'generate-password';

import UserModel from "../models/user"
import RoleModel from "../models/role";

let Role, User;

var transporter = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    secure: config.mail.secure, // use SSL
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
});

//let adminController = function (Src, template, middleware){
export default (Src, template, middleware) => class AdminController extends Controller {


  constructor() {
    super()
    Role = RoleModel(Src.DB.sequelize);
    User = UserModel(Src.DB.sequelize);
    User.belongsTo(Role, { foreignKey: 'role_id' });

  }
   
 

  list(req, res, next) {

      User.findAll({
        where: {role_id:[1,2]},
        include: [{ model: Role }],
        order:[['id','ASC']]
      }).then(result=>{
          template.data = {admins: result};
          template.layout.view = 'admin/list';
          res.render(template.layout.default, template);
      });
      
  }

  async add(req, res, next) {

      let roles = await Role.findAll({
        where:{id:[1,2]},
        order: [['name', 'DESC']]
      });

      template.data = {roles: roles};
      template.layout.view = 'admin/add';
      res.render(template.layout.default, template);

   }

  addsave(req, res, next) {

      let $data = req.body.data;
      let password = generator.generate({length: 8, numbers: true});
      let email = $data.email;
      $data.password = password;
      
      $data.token = sha1(crypto.createHash('md5').update($data.email).digest("hex"));
      
      User.create($data)
      .then(result => {
          let $html = 'Los datos de su cuenta son:<br><br>';
          $html += '<b>Correo:</b> ' + email + '<br>';
          $html += '<b>Contraseña:</b> ' + password + '<br><br>';
          $html += 'Para validar su cuenta ingrese al siguiente <a href="' + template.view.endpoint('/activation') + '?t=' + result.id + '.' + $data.token + '">Link</a>.';

          let mailOptions = {
            from: `"Agrintell" ${config.mail.user}`, // sender address (who sends)
            to: $data.email, // list of receivers (who receives)
            subject: `Infomación de Cuenta Administrativa`, // Subject line
            text: `Los datos de su cuenta son, Correo: ${email}, Contraseña: ${password}`, // plaintext body
            html: $html // html body
          };
          transporter.sendMail(mailOptions, function(error, info){
            if(error){
              template.view.flash.set('success', 'El usuario fue creado pero hubo problemas en el envió de su cuenta', template.view);
              return res.redirect('/admin/list');
            } else {
              template.view.flash.set('success', 'El usuario fue creado, se le envió un correo electrónico con los datos de su cuenta', template.view);
              return res.redirect('/admin/list');
            }
          });
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/admin/add');
      });

  }

  profile(req, res, next) {

    User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
    .then(result => {
      result.dataValues.created = moment(result.dataValues.created).format('Y-MM-DD');
      template.data = {admin: result};
      template.layout.view = 'admin/profile';
      res.render(template.layout.default, template);
    });

  }

  async profile_edit(req, res, next) {

      let roles = await Role.findAll({
        where:{id:[1,2]},
        order: [['name', 'DESC']]
      });

      User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
      .then(result => {
         template.data = {admin: result, roles: roles};
         template.layout.view = 'admin/profile_edit';
        res.render(template.layout.default, template);
      });

  }

  async profile_editsave(req, res, next) {

      let $data = req.body.data;
      $data.active = ('active' in $data) ? true : false;
      $data.id = req.params.id;

      User.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios de su cuenta', template.view);
          return res.redirect('/admin/profile/' + req.params.id);
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/admin/profile/edit/' + req.params.id);
      });

  }

  async view(req, res, next) {

    User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
    .then(result => {
      result.dataValues.created = moment(result.dataValues.created).format('Y-MM-DD');
      template.data = {admin: result};
      template.layout.view = 'admin/view';
      res.render(template.layout.default, template);
    });

  }

  async edit(req, res, next) {

      let roles = await Role.findAll({
        where:{id:[1,2]},
        order: [['name', 'DESC']]
      });

      User.findOne({where: {id: parseInt(req.params.id)}, include: [{ model: Role }]})
      .then(result => {
         template.data = {admin: result, roles: roles};
         template.layout.view = 'admin/edit';
        res.render(template.layout.default, template);
      });

  }

  editsave(req, res, next) {

      let $data = req.body.data;
      $data.active = ('active' in $data) ? true : false;
      $data.id = req.params.id;

      User.update($data, {where: {id: req.params.id} } )
      .then(result => {
          template.view.flash.set('success', 'Se realizó los cambios al usuario', template.view);
          return res.redirect('/admin/list');
      }).catch(Sequelize.ValidationError, function (msg) {          
          template.view.flash.set('error', msg.errors[0].message, template.view);
          return res.redirect('/admins/edit/' + req.params.id);
      });

  }

  delete(req, res, next) {
      User.destroy({where: {id: req.params.id}})
      .then(result =>{
          template.view.flash.set('success', 'Se eliminó el usuario', template.view);
         return res.redirect('/admin/list');
      });

  }

  changepassword(req, res, next) {

      template.layout.view = 'admin/changepassword';
      res.render(template.layout.default, template);

  }

  changepassword_save(req, res, next) {
      
      let $data = req.body.data;
      if($data.newpassword == $data.confirmpassword) {
        User.findOne({
           where: {id:req.user.id, password: sha1($data.password)}
        })
        .then(result => {
            if(result) {
                result.password = $data.newpassword;
                result.save()
                .then(result => {
                    template.view.flash.set('success', 'La constraseña fue cambiada', template.view);
                    return res.redirect('/home/admin');
                }).catch(Sequelize.ValidationError, function (msg) {
                    template.view.flash.set('error', msg.errors[0].message, template.view);
                    return res.redirect('/admin/changepassword');
                    //return res.status(422).send(err.errors);
                });
            } else {
                template.view.flash.set('error', 'La contraseña actual no coincide', template.view);
                return res.redirect('/admin/changepassword');
            }
        });
      } else {
          template.view.flash.set('error', 'La nueva contraseña no es igual a la confirmación', template.view);
          return res.redirect('/admin/changepassword');
      }

  }

}

