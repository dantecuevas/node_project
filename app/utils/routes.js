
module.exports = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  "/testroute":
              {type: 'GET', action: 'TestController@index', allow: true},

  // HOME CONTROLLER
  "/":
              {type: 'GET', action: 'HomeController@index', allow: true},
  "/home/admin":
              {type: 'GET', action: 'HomeController@admin'},
  "/home/farmer":
              {type: 'GET', action: 'HomeController@farmer'},

  // USER CONTROLLER
  "/login":
              {type: 'GET', action: 'UserController@login', allow: true},
  "/loginvalidate":
              {type: 'POST', action: 'UserController@loginvalidate', allow: true},
  "/user/register":
              {type: 'GET', action: 'UserController@register', allow: true},
  "/user/registersave":
              {type: 'POST', action: 'UserController@registersave', allow: true},
  "/logout":
              {type: 'GET', action: 'UserController@logout', allow: true},
  "/forgetpassword":
              {type: 'GET', action: 'UserController@forgetpassword', allow: true},
  "/forgetpasswordsave":
              {type: 'POST', action: 'UserController@forgetpasswordsave', allow: true},
  "/changepassword":
              {type: 'GET', action: 'UserController@changepassword', allow: true},
  "/changepasswordsave":
              {type: 'POST', action: 'UserController@changepasswordsave', allow: true},
  "/activation":
              {type: 'GET', action: 'UserController@activation', allow: true},
  "/getstates":
              {type: 'POST', action: 'UserController@getstates', allow: true},
  "/getcities":
              {type: 'POST', action: 'UserController@getcities', allow: true},

  // ADMIN CONTROLLER
  "/admin/list":
              {type: 'GET', action: 'AdminController@list'},
  "/admin/add":
              {type: 'GET', action: 'AdminController@add'},
  "/admin/addsave":
              {type: 'POST', action: 'AdminController@addsave'},
  "/admin/profile/:id":
              {type: 'GET', action: 'AdminController@profile'},
  "/admin/profile/edit/:id":
              {type: 'GET', action: 'AdminController@profile_edit'},
  "/admin/profile/editsave/:id":
              {type: 'POST', action: 'AdminController@profile_editsave'},
  "/admin/view/:id":
              {type: 'GET', action: 'AdminController@view'},
  "/admin/edit/:id":
              {type: 'GET', action: 'AdminController@edit'},
  "/admin/editsave/:id":
              {type: 'POST', action: 'AdminController@editsave'},
  "/admin/delete/:id":
              {type: 'POST', action: 'AdminController@delete'},
  "/admin/changepassword":
              {type: 'GET', action: 'AdminController@changepassword'},
  "/admin/changepasswordsave":
              {type: 'POST', action: 'AdminController@changepassword_save'},

  // FARMER CONTROLLER              
  "/farmer/list":
              {type: 'GET', action: 'FarmerController@list'},
  "/farmer/view/:id":
              {type: 'GET', action: 'FarmerController@view'},
  "/farmer/edit/:id":
              {type: 'GET', action: 'FarmerController@edit'},
  "/farmer/editsave/:id":
              {type: 'POST', action: 'FarmerController@editsave'},
  "/farmer/profile/:id":
              {type: 'GET', action: 'FarmerController@profile'},
  "/farmer/profile/edit/:id":
              {type: 'GET', action: 'FarmerController@profilesave'},
  "/farmer/profile/editsave/:id":
              {type: 'POST', action: 'FarmerController@profile_editsave'},
  "/farmer/delete/:id":
              {type: 'POST', action: 'FarmerController@delete'},            
  "/farmer/changepassword":
              {type: 'GET', action: 'FarmerController@changepassword'},
  "/farmer/changepasswordsave":
              {type: 'POST', action: 'FarmerController@changepassword_save'},
  "/farmer/map":
              {type: 'GET', action: 'FarmerController@map'},
  "/farmer/deletePolygonAll":
              {type: 'POST', action: 'FarmerController@deletePolygonAll'},
  "/farmer/deleteMakerField":
              {type: 'POST', action: 'FarmerController@deleteMakerField'},

  // FARM CONTROLLER              

  "/farm/savePolygon":
              {type: 'POST', action: 'FarmController@savePolygon'},
  "/farm/view":
              {type: 'GET', action: 'FarmController@view'},  
  "/farm/changefield":
              {type: 'GET', action: 'FarmController@changefield'},  
  "/farm/generatepoints":
              {type: 'GET', action: 'FarmController@generatepoints'},
  "/farm/generatedevices":
              {type: 'GET', action: 'FarmController@generatedevices'},
  "/farm/savePoint":
              {type: 'POST', action: 'FarmController@savePoint'},
  "/farm/saveDevice":
              {type: 'POST', action: 'FarmController@saveDevice'},
  "/farm/deletePoint":
              {type: 'POST', action: 'FarmController@deletePoint'},
  "/farm/deleteDevice":
              {type: 'POST', action: 'FarmController@deleteDevice'},            
  "/farm/detailPoint/:id":
              {type: 'GET', action: 'FarmController@detailPoint'},
  "/farm/detailDevice/:id":
              {type: 'GET', action: 'FarmController@detailDevice'},
  "/farm/detailPointEdit":
              {type: 'POST', action: 'FarmController@detailPointEdit'},            
  "/farm/detailPointSave":
              {type: 'POST', action: 'FarmController@detailPointSave'},            
  "/farm/deletePolygonAndPoint":
              {type: 'POST', action: 'FarmController@deletePolygonAndPoint'},
  "/farm/getDevices":
              {type: 'POST', action: 'FarmController@getDevices'},   
  "/farm/savefield_detail":         
              {type: 'POST', action: 'FarmController@savefield_detail'},
  "/farm/getdatapolygon/:id":
              {type: 'GET', action: 'FarmController@getdatapolygon'},
  


  // DEVICE CONTROLLER
  "/device/list":
              {type: 'GET', action: 'DeviceController@list'},
  "/device/view/:id":
              {type: 'GET', action: 'DeviceController@view'},
  "/device/add":
              {type: 'GET', action: 'DeviceController@add'},
  "/device/edit/:id":
              {type: 'GET', action: 'DeviceController@edit'},
  "/device/editsave/:id":
              {type: 'POST', action: 'DeviceController@editsave'},
  "/device/addsave":
              {type: 'POST', action: 'DeviceController@addsave'},
  "/device/validate":
              {type: 'GET', action: 'DeviceController@validate'},
  "/device/validatesave":
              {type: 'POST', action: 'DeviceController@validatesave'},
  "/device/sensor":
              {type: 'GET', action: 'DeviceController@sensor'},
  "/device/valves":
              {type: 'GET', action: 'DeviceController@valves'}, 
  "/device/gateway":
              {type: 'GET', action: 'DeviceController@gateway'},
  "/device/delete/:id":
              {type: 'POST', action: 'DeviceController@delete'},
  "/device/editValve/:id":
              {type: 'GET', action: 'DeviceController@editValve'},
  "/device/editvalvesave/:id":
              {type: 'POST', action: 'DeviceController@editvalvesave'},
  "/device/groups":
              {type: 'GET', action: 'DeviceController@groups'},
  "/device/addgroup":
              {type: 'POST', action: 'DeviceController@addgroup'},
  "/device/deletegroup/:id":
              {type: 'POST', action: 'DeviceController@deletegroup'},
  "/device/devicebygroup":
              {type: 'GET', action: 'DeviceController@devicebygroup'},
  "/device/dropgroup/:id":
              {type: 'POST', action: 'DeviceController@dropgroup'},  
  "/api/v1/device/devices-add-remote":
              {type: 'POST', action: 'DeviceController@devicesAddRemote', allow: true},
  "/api/v1/device/devices-add-local":
              {type: 'POST', action: 'DeviceController@devicesAddLocal', allow: true},    
  "/device/configuration/:id":
              {type: 'GET', action: 'DeviceController@configuration'},
  "/device/getvalues/:id":
              {type: 'GET', action: 'DeviceController@getvalues'},         
  "/device/savegetvalues":
              {type: 'POST', action: 'DeviceController@savegetvalues'},
  "/device/detail/active/:id":
              {type: 'POST', action: 'DeviceController@detailActive'},
  "/device/graph/:id":
              {type: 'GET', action: 'DeviceController@graph'},
  "/device/graph2/:id":
              {type: 'GET', action: 'DeviceController@graph2'},
  "/device/graph_from_device1/:id":
              {type: 'GET', action: 'DeviceController@graph_from_device1'},
  "/device/graph_from_device2/:id":
              {type: 'GET', action: 'DeviceController@graph_from_device2'},
  "/device/getdatafilters":
              {type: 'GET', action: 'DeviceController@getdatafilters'},  
  "/device/save3values/:id":
              {type: 'POST', action: 'DeviceController@save3values'},  
  "/device/valve/:id":
              {type: 'GET', action: 'DeviceController@valve'},
  "/device/editgroup/:id":
              {type: 'GET', action: 'DeviceController@editgroup'},
  "/device/editgroupsave/:id":
              {type: 'POST', action: 'DeviceController@editgroupsave'},
  "/device/groupbyirrigation":
              {type: 'GET', action: 'DeviceController@groupbyirrigation'},
  "/device/dropirrigation/:id":
              {type: 'POST', action: 'DeviceController@dropirrigation'},  
  "/device/deleteirrigation/:id":
              {type: 'POST', action: 'DeviceController@deleteirrigation'},
  "/device/download/:id":
              {type: 'POST', action: 'DeviceController@download'},


  // FIELD CONTROLLER
  "/field/list":
              {type: 'GET', action: 'FieldController@list'},
  "/field/add":
              {type: 'GET', action: 'FieldController@add'},
  "/field/addsave":
              {type: 'POST', action: 'FieldController@addsave'},
  

  
  // IRRIGATION CONTROLLER
  "/calendar/view":
              {type: 'GET', action: 'CalendarController@view'},
  "/calendar/addirrigation":
              {type: 'POST', action: 'CalendarController@addirrigation'},
  "/calendar/newScheduleIrrigation":
              {type: 'GET', action: 'CalendarController@newScheduleIrrigation'},
  "/calendar/eventsave":
              {type: 'POST', action: 'CalendarController@eventsave'},
  "/calendar/getgroups":
              {type: 'POST', action: 'CalendarController@getgroups'},
  "/calendar/getgroupsdata":
              {type: 'POST', action: 'CalendarController@getgroupsdata'},
  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

  //'POST /projects/addprojectaction':                   { action: 'projects/add-project-action' },


  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝
  //'/terms':                   '/legal/terms',
  //'/logout':                  '/api/v1/account/logout',

}
