import routes from './routes'

const controllersDir = __dirname+'/../controllers/'
let fs = require('fs')
let Controllers = {}

let controllers = function(Router, Source, template, midlewares){

	fs.readdirSync(controllersDir).forEach(file => {
	
		file=file.split(".")[0]
		console.log(file);
		if(file!='Controller'){
			console.log("Entro: "+file);
			let Controller = require(__dirname+'/../controllers/' + file)
			//import Controller from './app/controllers/' + file
			let Ctrll = Controller(Source, template, midlewares)
			//let T = new Test()
			Controllers[file] = new Ctrll()
		}

	});

	let middlewares = [midlewares.auth, midlewares.acl]

	for (var key in routes) {
		console.log(key + ": " + routes[key].action)

		let $action = routes[key].action.split("@")
		if(Controllers[$action[0]]){
			if(Controllers[$action[0]][$action[1]]){

				switch(routes[key].type) {
				  case 'GET':
				    //Router.get(key, Controllers[$action[0]][$action[1]])
				    Source.app.get(key, middlewares, Controllers[$action[0]][$action[1]])
				    break;
				  case 'POST':
				    //Router.post(key, Controllers[$action[0]][$action[1]])
				    Source.app.post(key, middlewares, Controllers[$action[0]][$action[1]])
				    break;
				  default:
				    throw new Error('Method '+routes[key].type+' to '+key+' is incorrect in routes.js');
				}
				
			}else {
				throw new Error('Action ' +$action[1]+' for Controller '+$action[0]+' is incorrect in routes.js');
			}
		} else {
			throw new Error('Controller ' + $action[0]+' is incorrect in routes.js');
		}

	}

}

export default controllers



